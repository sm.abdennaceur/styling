import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AuthentificationService } from "./authentification.service";
import { blankUser, testUserData, validUser } from '~app/mocks';

import { environment } from '~environments/environment';


describe('AuthentificationServiceSpec', () => {
  let injector;
  let service: AuthentificationService;
  let httpMock: HttpTestingController;
  let apiUrl = environment.apiUrl
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthentificationService]
    });

    injector = getTestBed();
    service = injector.get(AuthentificationService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should perform a post to /auth with email and password', () => {
    service.login(validUser.email, validUser.password, 'true').subscribe(result => {
      expect(result.length).toBe(3);
      expect(result).toEqual(testUserData);
    });

    const req = httpMock.expectOne(apiUrl + '/logon/auth');
    expect(req.request.method).toBe('POST');
    req.flush(testUserData);
  });

  it('should Not perform a post to /auth with email and password null', () => {
    service.login(blankUser.email, blankUser.password, 'true').subscribe(result => {
      expect(result.length).toBe(0);
      expect(result).toEqual([]);
    });

    const req = httpMock.expectOne(apiUrl + '/logon/auth');
    expect(req.request.method).toBe('POST');
    req.flush([]);
  });

})
