import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { DynamicNavMenuComponent } from './dynamic-nav-menu/dynamic-nav-menu.component';
import { TranslateModule } from '@ngx-translate/core';
import { LoadingPlaceholderComponent } from './loading-placeholder/loading-placeholder.component';
import { FeatherIconsModule } from './feather-icons/feather-icons.module';
import { RouterModule } from '@angular/router';

import { NgxSpinnerModule } from "ngx-spinner";
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { EmployeeDirectoryComponent } from './employee-directory/employee-directory.component';
import {MaterialModule} from "~app/material/material.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchPipe } from './search.pipe';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    DynamicFormComponent,
    DynamicNavMenuComponent,
    LoadingPlaceholderComponent,
    BreadcrumbComponent,
    EmployeeDirectoryComponent,
    SearchPipe,
    ConfirmDialogComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    NgxSpinnerModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
  ],
  exports: [
    DynamicFormComponent,
    DynamicNavMenuComponent,
    LoadingPlaceholderComponent,
    FeatherIconsModule,
    NgxSpinnerModule,
    BreadcrumbComponent,
    EmployeeDirectoryComponent
  ]
})
export class SharedModule { }
