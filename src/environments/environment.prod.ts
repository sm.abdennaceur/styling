export const environment = {
  name:'production',
  production: true,
  altImage:'prod',
  imageLogo:'https://rh.azuneed.com/resources/img/azuneed_logo_removebg.png',
  loginBgImg: 'url(assets/images/header_circlessvg.svg)',
  apiUrl:"http://localhost:8080/azuneed/rest",
  mediaUrl:"",
  uploadUrl:"",
  snackBarDuration:5000,
  BaseUrlBack: 'http://localhost:8080/azuneed/'
};
