import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HasUserProfilePermissionDirective } from './directives/has-user-profile-permission.directive';
import { DirectionService } from './service/direction.service';
import { RightSidebarService } from './service/rightsidebar.service';



@NgModule({
  declarations: [HasUserProfilePermissionDirective],
  imports: [
    CommonModule
  ],
  providers:[DirectionService,RightSidebarService],
  exports:[HasUserProfilePermissionDirective]
})
export class CoreModule { }
