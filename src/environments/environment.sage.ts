export const environment = {
    name:'sage',
    production: true,
    altImage:'sage',
    imageLogo:'/assets/images/logo-sage.svg',
    loginBgImg: 'url(assets/images/header_circlessvg.svg)',
    snackBarDuration: 5000,
    apiUrl:"http://localhost:8080/azuneed/rest",
    mediaUrl:"",
    uploadUrl:"",
    BaseUrlBack: 'http://localhost:8080/sage/'
  };
  