import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UserService} from "~core/http/user.service";
import {AuthentificationService} from "~core/http/authentification.service";
import { environment } from 'src/environments/environment';
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {UtilsService} from "~core/helpers/utils.service";
import {ConfirmDialogComponent} from "~shared/confirm-dialog/confirm-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {zip} from "rxjs";
import {TranslateService} from "@ngx-translate/core";
import {SelectionModel} from "@angular/cdk/collections";
import {DynamicFormComponent} from "~shared/dynamic-form/dynamic-form.component";
import { FilterService } from '~core/service/filter.service';
@Component({
  selector: 'app-employee-directory',
  templateUrl: './employee-directory.component.html',
  styleUrls: ['./employee-directory.component.scss']
})
export class EmployeeDirectoryComponent implements OnInit ,AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['userFirstName', 'userProfileLabel', 'userWorkCategory','userFunction', 'userHiringDate','action'];
  public dataSource = new MatTableDataSource<any>();
  selection = new SelectionModel<any>(true, []);
  employees: any
  currentUser: any
  filterAnnuaire: any = []
  url = environment.BaseUrlBack
  loading: false ;
  total: any;
  offset = 0;
  limit =5;
  type: any;
  isLoading = true;
  previousSize: any
  contentFilter = {"code":2000,"developerMessage":null,"link":"","message":[[{"companyOrgIndex":null,"filterItems":[{"detail":"TestLjaSte","displayPosition":null,"filterType":"CompanyBean","icon":null,"id":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idParent":"root"}],"filterLabel":"Sociétés/filiales","filterType":"CompanyBean"},{"companyOrgIndex":0,"filterItems":[{"detail":"keyrus","displayPosition":0,"filterType":"CompanyOrganization","icon":null,"id":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]3d004875-7ed5-348a-eedc-f35934138af5[]4560db67-09da-234d-9361-f75f387a8f52","idParent":"root"}],"filterLabel":"Sociétés","filterType":"CompanyOrganization"},{"companyOrgIndex":1,"filterItems":[{"detail":"Etablissement 1","displayPosition":0,"filterType":"CompanyOrganization","icon":null,"id":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]c2a321bc-9aea-5393-c838-6162f26771cc[]8a908d1b-2cbb-dffc-7f12-ab18c2868865","idParent":"root"}],"filterLabel":"Etablissements","filterType":"CompanyOrganization"},{"companyOrgIndex":2,"filterItems":[{"detail":"Département 1","displayPosition":0,"filterType":"CompanyOrganization","icon":null,"id":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]9c6afc09-e326-2f97-4a77-d251efdcd1d0[]28631aae-d5ab-b349-0cfb-7def7026a7bb","idParent":"root"}],"filterLabel":"Départements","filterType":"CompanyOrganization"},{"companyOrgIndex":3,"filterItems":[{"detail":"Service 1","displayPosition":0,"filterType":"CompanyOrganization","icon":null,"id":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]f5e5f937-0afa-8a4b-724d-114ac333b2e8[]36a5b063-ddb0-9304-e15f-f27374964116","idParent":"root"}],"filterLabel":"Services","filterType":"CompanyOrganization"},{"companyOrgIndex":null,"filterItems":[{"detail":"MANAGER manager","displayPosition":null,"filterType":"UserBean","icon":"<img src='resources/img/anonym_person_200x200.png'/>","id":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]ac5200f8-7fed-4578-8ce1-addbb7186d0b","idParent":"root"},{"detail":"SADFI amine","displayPosition":null,"filterType":"UserBean","icon":"<img src='resources/img/anonym_person_200x200.png'/>","id":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]142b4271-3e30-411b-bff1-fa36b009deb8","idParent":"root"},{"detail":"TESTLJASTE TestLjaSte","displayPosition":null,"filterType":"UserBean","icon":"<img src='resources/img/anonym_person_200x200.png'/>","id":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]e93ae142-47f1-49c9-b7bf-335480d1499d","idParent":"root"},{"detail":"TESTROLE testRole","displayPosition":null,"filterType":"UserBean","icon":"<img src='resources/img/anonym_person_200x200.png'/>","id":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]25e2925b-90d4-4f02-afc0-822192fcdfa3","idParent":"root"}],"filterLabel":"Salariés ","filterType":"UserBean"},{"companyOrgIndex":null,"filterItems":[{"detail":"Responsable RH","displayPosition":null,"filterType":"appRole","icon":"","id":"hr","idParent":"root"},{"detail":"Administrateur","displayPosition":null,"filterType":"appRole","icon":"","id":"admin","idParent":"root"},{"detail":"Responsable Paye","displayPosition":null,"filterType":"appRole","icon":"","id":"payroll","idParent":"root"}],"filterLabel":"Fonction pour l'application ","filterType":"appRole"}]],"status":200,"totalCount":0};
  dataFake = {"code":2000,"link":"","message":[[{"anHrResponsible":true,"applyVisibilityWithNoUserDirectory":null,"appRole":";hr;admin;payroll","azuneedInfo":null,"bypassSync":null,"byPassWf":null,"companyDetail":"TestLjaSte","country":null,"currency":null,"dataPermissionId":null,"dataValues":[],"dateCreate":null,"dateUpdate":null,"deletedLinkedManagerRelation":[],"deletedLinkedUserRelation":[],"externalAppSettings":null,"fireBaseAuthToken":null,"flagEmailAddressHasChanged":false,"formMode":null,"globalBaremId":null,"hasAccessToAzuneedPortal":false,"hasBeenDeleted":false,"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idUser":"ac5200f8-7fed-4578-8ce1-addbb7186d0b","parentclass":"com.azuneed.backoffice.appentity.user.UserBean","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]ac5200f8-7fed-4578-8ce1-addbb7186d0b"},"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idUser":"ac5200f8-7fed-4578-8ce1-addbb7186d0b","idUserProfile":null,"internalCode":null,"isActive":null,"isAnAdmin":null,"isArchived":null,"jenjiId":"2e7620aa-835e-457c-8844-f9a8953605af","linkedManagerRelation":[{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idUser":"ac5200f8-7fed-4578-8ce1-addbb7186d0b","linkedManagerIdCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","linkedManagerIdUser":"e93ae142-47f1-49c9-b7bf-335480d1499d","parentclass":"com.azuneed.backoffice.appentity.user.LinkedManagerRelation","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]ac5200f8-7fed-4578-8ce1-addbb7186d0b[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]e93ae142-47f1-49c9-b7bf-335480d1499d"},"linkedManagerName":"TESTLJASTE TestLjaSte","linkedManagerPhoto":null,"linkedManagerRelation":"manager_1","modelListInstance":{}}],"linkedUserRelation":[],"loadIntro":false,"locale":null,"manageMealVoucher":true,"nationality":null,"paymentMethodId":null,"payrollResponsible":true,"qualification":null,"socialSecurityNumber":null,"tco":0.0,"thirdAccount":null,"userAddress":null,"userBirthDay":null,"userChildren":null,"userCompanyOrganizations":[{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idCompanyOrg":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idTreeBeanOrg":"3d004875-7ed5-348a-eedc-f35934138af5","idTreeBeanOrgItem":"4560db67-09da-234d-9361-f75f387a8f52","idUser":"ac5200f8-7fed-4578-8ce1-addbb7186d0b","parentclass":"com.azuneed.backoffice.appentity.user.UserCompanyOrganization","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]ac5200f8-7fed-4578-8ce1-addbb7186d0b[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]3d004875-7ed5-348a-eedc-f35934138af5[]4560db67-09da-234d-9361-f75f387a8f52"},"insertedByLabel":false,"modelListInstance":{},"userCompanyOrganizationItemLabel":"keyrus","userCompanyOrganizationLabel":"Sociétés"},{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idCompanyOrg":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idTreeBeanOrg":"c2a321bc-9aea-5393-c838-6162f26771cc","idTreeBeanOrgItem":"8a908d1b-2cbb-dffc-7f12-ab18c2868865","idUser":"ac5200f8-7fed-4578-8ce1-addbb7186d0b","parentclass":"com.azuneed.backoffice.appentity.user.UserCompanyOrganization","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]ac5200f8-7fed-4578-8ce1-addbb7186d0b[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]c2a321bc-9aea-5393-c838-6162f26771cc[]8a908d1b-2cbb-dffc-7f12-ab18c2868865"},"insertedByLabel":false,"modelListInstance":{},"userCompanyOrganizationItemLabel":"Etablissement 1","userCompanyOrganizationLabel":"Etablissements"},{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idCompanyOrg":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idTreeBeanOrg":"9c6afc09-e326-2f97-4a77-d251efdcd1d0","idTreeBeanOrgItem":"28631aae-d5ab-b349-0cfb-7def7026a7bb","idUser":"ac5200f8-7fed-4578-8ce1-addbb7186d0b","parentclass":"com.azuneed.backoffice.appentity.user.UserCompanyOrganization","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]ac5200f8-7fed-4578-8ce1-addbb7186d0b[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]9c6afc09-e326-2f97-4a77-d251efdcd1d0[]28631aae-d5ab-b349-0cfb-7def7026a7bb"},"insertedByLabel":false,"modelListInstance":{},"userCompanyOrganizationItemLabel":"Département 1","userCompanyOrganizationLabel":"Départements"},{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idCompanyOrg":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idTreeBeanOrg":"f5e5f937-0afa-8a4b-724d-114ac333b2e8","idTreeBeanOrgItem":"36a5b063-ddb0-9304-e15f-f27374964116","idUser":"ac5200f8-7fed-4578-8ce1-addbb7186d0b","parentclass":"com.azuneed.backoffice.appentity.user.UserCompanyOrganization","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]ac5200f8-7fed-4578-8ce1-addbb7186d0b[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]f5e5f937-0afa-8a4b-724d-114ac333b2e8[]36a5b063-ddb0-9304-e15f-f27374964116"},"insertedByLabel":false,"modelListInstance":{},"userCompanyOrganizationItemLabel":"Service 1","userCompanyOrganizationLabel":"Services"}],"userCreate":null,"userDatas":{"groups":[],"sysDataRights":{"mapProperty":{}}},"userDaysOffId":null,"userDelegations":null,"userDepartureDate":null,"userDirectories":null,"userEchelon":null,"userEmail":"ljastetest@manager.com","userEmailTempKey":null,"userExtraWorkedDays":null,"userFirstName":"manager","userFunction":null,"userGender":null,"userHiringDate":null,"userIbanItems":null,"userLastName":"MANAGER","userLeaveVacationProfiles":null,"userLinkConnection":"https://rh.azuneed.com?userEmail=ljastetest@manager.com&tempToken=GfwfYzL1nxMcp5yvlOHfNQ3kADHonkTDa6zYIiJwJ-I_Lao2cqQfXMLdHKYEzFYynlcQNms_051jdwJPvxlPZTEzMTc3MDMw","userLocale":null,"userMaritalStatus":null,"userPayroll":null,"userPhones":[],"userPhoto":"resources/img/anonym_person_200x200.png","userPhotoFileBeanId":null,"userPhotoHasChanged":false,"userProducts":null,"userProfileLabel":null,"userPwd":null,"userSeniorityDate":null,"userSeniorityMonthToAdd":null,"userSerial":null,"userThirdApp":[],"userTimeZone":null,"userUpdate":null,"userWellBeingVotes":null,"userWidgetsPref":[],"userWorkCategory":null,"userWorkContracts":null,"userWorkedHours":null,"userWorkLevel":null,"workContractPackage":null,"workContractPackageNbUnit":null},{"anHrResponsible":true,"applyVisibilityWithNoUserDirectory":null,"appRole":";admin;payroll;hr","azuneedInfo":null,"bypassSync":null,"byPassWf":null,"companyDetail":"TestLjaSte","country":null,"currency":null,"dataPermissionId":null,"dataValues":[],"dateCreate":null,"dateUpdate":null,"deletedLinkedManagerRelation":[],"deletedLinkedUserRelation":[],"externalAppSettings":null,"fireBaseAuthToken":null,"flagEmailAddressHasChanged":false,"formMode":null,"globalBaremId":null,"hasAccessToAzuneedPortal":false,"hasBeenDeleted":false,"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idUser":"142b4271-3e30-411b-bff1-fa36b009deb8","parentclass":"com.azuneed.backoffice.appentity.user.UserBean","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]142b4271-3e30-411b-bff1-fa36b009deb8"},"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idUser":"142b4271-3e30-411b-bff1-fa36b009deb8","idUserProfile":null,"internalCode":null,"isActive":null,"isAnAdmin":null,"isArchived":null,"jenjiId":"e2dbbac8-0dff-4acf-b8f5-6eb63bfb0fbb","linkedManagerRelation":[],"linkedUserRelation":[],"loadIntro":false,"locale":null,"manageMealVoucher":true,"nationality":null,"paymentMethodId":null,"payrollResponsible":true,"qualification":null,"socialSecurityNumber":null,"tco":0.0,"thirdAccount":null,"userAddress":null,"userBirthDay":null,"userChildren":null,"userCompanyOrganizations":[{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idCompanyOrg":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idTreeBeanOrg":"3d004875-7ed5-348a-eedc-f35934138af5","idTreeBeanOrgItem":"4560db67-09da-234d-9361-f75f387a8f52","idUser":"142b4271-3e30-411b-bff1-fa36b009deb8","parentclass":"com.azuneed.backoffice.appentity.user.UserCompanyOrganization","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]142b4271-3e30-411b-bff1-fa36b009deb8[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]3d004875-7ed5-348a-eedc-f35934138af5[]4560db67-09da-234d-9361-f75f387a8f52"},"insertedByLabel":false,"modelListInstance":{},"userCompanyOrganizationItemLabel":"keyrus","userCompanyOrganizationLabel":"Sociétés"},{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idCompanyOrg":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idTreeBeanOrg":"c2a321bc-9aea-5393-c838-6162f26771cc","idTreeBeanOrgItem":"8a908d1b-2cbb-dffc-7f12-ab18c2868865","idUser":"142b4271-3e30-411b-bff1-fa36b009deb8","parentclass":"com.azuneed.backoffice.appentity.user.UserCompanyOrganization","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]142b4271-3e30-411b-bff1-fa36b009deb8[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]c2a321bc-9aea-5393-c838-6162f26771cc[]8a908d1b-2cbb-dffc-7f12-ab18c2868865"},"insertedByLabel":false,"modelListInstance":{},"userCompanyOrganizationItemLabel":"Etablissement 1","userCompanyOrganizationLabel":"Etablissements"},{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idCompanyOrg":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idTreeBeanOrg":"9c6afc09-e326-2f97-4a77-d251efdcd1d0","idTreeBeanOrgItem":"28631aae-d5ab-b349-0cfb-7def7026a7bb","idUser":"142b4271-3e30-411b-bff1-fa36b009deb8","parentclass":"com.azuneed.backoffice.appentity.user.UserCompanyOrganization","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]142b4271-3e30-411b-bff1-fa36b009deb8[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]9c6afc09-e326-2f97-4a77-d251efdcd1d0[]28631aae-d5ab-b349-0cfb-7def7026a7bb"},"insertedByLabel":false,"modelListInstance":{},"userCompanyOrganizationItemLabel":"Département 1","userCompanyOrganizationLabel":"Départements"},{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idCompanyOrg":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idTreeBeanOrg":"f5e5f937-0afa-8a4b-724d-114ac333b2e8","idTreeBeanOrgItem":"36a5b063-ddb0-9304-e15f-f27374964116","idUser":"142b4271-3e30-411b-bff1-fa36b009deb8","parentclass":"com.azuneed.backoffice.appentity.user.UserCompanyOrganization","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]142b4271-3e30-411b-bff1-fa36b009deb8[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]f5e5f937-0afa-8a4b-724d-114ac333b2e8[]36a5b063-ddb0-9304-e15f-f27374964116"},"insertedByLabel":false,"modelListInstance":{},"userCompanyOrganizationItemLabel":"Service 1","userCompanyOrganizationLabel":"Services"}],"userCreate":null,"userDatas":{"groups":[],"sysDataRights":{"mapProperty":{}}},"userDaysOffId":null,"userDelegations":null,"userDepartureDate":null,"userDirectories":null,"userEchelon":null,"userEmail":"amine.sadfi@keyrus.com","userEmailTempKey":null,"userExtraWorkedDays":null,"userFirstName":"amine","userFunction":"Full-stack developer","userGender":null,"userHiringDate":1648767600000,"userIbanItems":null,"userLastName":"SADFI","userLeaveVacationProfiles":null,"userLinkConnection":"https://rh.azuneed.com?userEmail=amine.sadfi@keyrus.com&tempToken=1v9kOuARxS0qiPTqDrONq_SXz3F1MOOQdjI-BvE6eKrLpp8XkDodXqpVaZ05Vv3Xvlextx2-uhOFQGZTG2QrRzUwMTY3NTI3","userLocale":null,"userMaritalStatus":null,"userPayroll":null,"userPhones":[],"userPhoto":"resources/img/anonym_person_200x200.png","userPhotoFileBeanId":null,"userPhotoHasChanged":false,"userProducts":null,"userProfileLabel":null,"userPwd":null,"userSeniorityDate":null,"userSeniorityMonthToAdd":null,"userSerial":null,"userThirdApp":[],"userTimeZone":null,"userUpdate":null,"userWellBeingVotes":null,"userWidgetsPref":[],"userWorkCategory":"frame","userWorkContracts":null,"userWorkedHours":null,"userWorkLevel":null,"workContractPackage":null,"workContractPackageNbUnit":null},{"anHrResponsible":true,"applyVisibilityWithNoUserDirectory":null,"appRole":";admin;hr","azuneedInfo":null,"bypassSync":null,"byPassWf":null,"companyDetail":"TestLjaSte","country":null,"currency":null,"dataPermissionId":null,"dataValues":[],"dateCreate":null,"dateUpdate":null,"deletedLinkedManagerRelation":[],"deletedLinkedUserRelation":[],"externalAppSettings":null,"fireBaseAuthToken":"dhM60UGVKsw:APA91bHXaQY4FOTKMuaoPeIQq8qQ6vLzsYPICBVlx_PlX_viH_wxCa4vXncbla0LgEfvrQhEWe5P66vioG1LeHrbRhAYhWiapha6R8zlx6BiHnZPom7uwOWPu37_DNWBH4hQpNIaFzq_","flagEmailAddressHasChanged":false,"formMode":null,"globalBaremId":null,"hasAccessToAzuneedPortal":true,"hasBeenDeleted":false,"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idUser":"e93ae142-47f1-49c9-b7bf-335480d1499d","parentclass":"com.azuneed.backoffice.appentity.user.UserBean","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]e93ae142-47f1-49c9-b7bf-335480d1499d"},"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idUser":"e93ae142-47f1-49c9-b7bf-335480d1499d","idUserProfile":null,"internalCode":null,"isActive":true,"isAnAdmin":null,"isArchived":null,"jenjiId":"d862854f-8647-40db-b7e7-946e3b6edd6d","linkedManagerRelation":[],"linkedUserRelation":[{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idUser":"e93ae142-47f1-49c9-b7bf-335480d1499d","linkedUserIdCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","linkedUserIdUser":"ac5200f8-7fed-4578-8ce1-addbb7186d0b","parentclass":"com.azuneed.backoffice.appentity.user.LinkedUserRelation","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]e93ae142-47f1-49c9-b7bf-335480d1499d[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]ac5200f8-7fed-4578-8ce1-addbb7186d0b"},"linkedUserName":"MANAGER manager","linkedUserPhoto":null,"linkedUserRelation":"manager_1","modelListInstance":{}}],"loadIntro":false,"locale":null,"manageMealVoucher":true,"nationality":null,"paymentMethodId":null,"payrollResponsible":false,"qualification":null,"socialSecurityNumber":null,"tco":0.0,"thirdAccount":null,"userAddress":null,"userBirthDay":null,"userChildren":null,"userCompanyOrganizations":[{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idCompanyOrg":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idTreeBeanOrg":"3d004875-7ed5-348a-eedc-f35934138af5","idTreeBeanOrgItem":"4560db67-09da-234d-9361-f75f387a8f52","idUser":"e93ae142-47f1-49c9-b7bf-335480d1499d","parentclass":"com.azuneed.backoffice.appentity.user.UserCompanyOrganization","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]e93ae142-47f1-49c9-b7bf-335480d1499d[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]3d004875-7ed5-348a-eedc-f35934138af5[]4560db67-09da-234d-9361-f75f387a8f52"},"insertedByLabel":false,"modelListInstance":{},"userCompanyOrganizationItemLabel":"keyrus","userCompanyOrganizationLabel":"Sociétés"},{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idCompanyOrg":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idTreeBeanOrg":"c2a321bc-9aea-5393-c838-6162f26771cc","idTreeBeanOrgItem":"8a908d1b-2cbb-dffc-7f12-ab18c2868865","idUser":"e93ae142-47f1-49c9-b7bf-335480d1499d","parentclass":"com.azuneed.backoffice.appentity.user.UserCompanyOrganization","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]e93ae142-47f1-49c9-b7bf-335480d1499d[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]c2a321bc-9aea-5393-c838-6162f26771cc[]8a908d1b-2cbb-dffc-7f12-ab18c2868865"},"insertedByLabel":false,"modelListInstance":{},"userCompanyOrganizationItemLabel":"Etablissement 1","userCompanyOrganizationLabel":"Etablissements"},{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idCompanyOrg":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idTreeBeanOrg":"9c6afc09-e326-2f97-4a77-d251efdcd1d0","idTreeBeanOrgItem":"28631aae-d5ab-b349-0cfb-7def7026a7bb","idUser":"e93ae142-47f1-49c9-b7bf-335480d1499d","parentclass":"com.azuneed.backoffice.appentity.user.UserCompanyOrganization","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]e93ae142-47f1-49c9-b7bf-335480d1499d[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]9c6afc09-e326-2f97-4a77-d251efdcd1d0[]28631aae-d5ab-b349-0cfb-7def7026a7bb"},"insertedByLabel":false,"modelListInstance":{},"userCompanyOrganizationItemLabel":"Département 1","userCompanyOrganizationLabel":"Départements"},{"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idCompanyOrg":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idTreeBeanOrg":"f5e5f937-0afa-8a4b-724d-114ac333b2e8","idTreeBeanOrgItem":"36a5b063-ddb0-9304-e15f-f27374964116","idUser":"e93ae142-47f1-49c9-b7bf-335480d1499d","parentclass":"com.azuneed.backoffice.appentity.user.UserCompanyOrganization","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]e93ae142-47f1-49c9-b7bf-335480d1499d[]2697b4b7-ea54-4cac-a57a-bce57ac146a6[]f5e5f937-0afa-8a4b-724d-114ac333b2e8[]36a5b063-ddb0-9304-e15f-f27374964116"},"insertedByLabel":false,"modelListInstance":{},"userCompanyOrganizationItemLabel":"Service 1","userCompanyOrganizationLabel":"Services"}],"userCreate":null,"userDatas":{"groups":[],"sysDataRights":{"mapProperty":{}}},"userDaysOffId":null,"userDelegations":null,"userDepartureDate":null,"userDirectories":null,"userEchelon":null,"userEmail":"TestLjaSte@lja.com","userEmailTempKey":null,"userExtraWorkedDays":null,"userFirstName":"TestLjaSte","userFunction":"","userGender":null,"userHiringDate":978303600000,"userIbanItems":null,"userLastName":"TESTLJASTE","userLeaveVacationProfiles":null,"userLinkConnection":"https://rh.azuneed.com?userEmail=TestLjaSte@lja.com&tempToken=ZS60elJ-HUeZgJ76oCGGd1qmJCMCPD7vKxMlg7ZMNRzxK0sOkqTucW5t17_5n8vU8fgC3hishAkyMjIxMzcwNA","userLocale":null,"userMaritalStatus":null,"userPayroll":null,"userPhones":[],"userPhoto":"resources/img/anonym_person_200x200.png","userPhotoFileBeanId":null,"userPhotoHasChanged":false,"userProducts":null,"userProfileLabel":null,"userPwd":null,"userSeniorityDate":null,"userSeniorityMonthToAdd":null,"userSerial":"334562","userThirdApp":[],"userTimeZone":null,"userUpdate":null,"userWellBeingVotes":null,"userWidgetsPref":[],"userWorkCategory":"frame","userWorkContracts":null,"userWorkedHours":null,"userWorkLevel":null,"workContractPackage":null,"workContractPackageNbUnit":null},{"anHrResponsible":false,"applyVisibilityWithNoUserDirectory":null,"appRole":null,"azuneedInfo":null,"bypassSync":null,"byPassWf":null,"companyDetail":"TestLjaSte","country":null,"currency":null,"dataPermissionId":null,"dataValues":[],"dateCreate":null,"dateUpdate":null,"deletedLinkedManagerRelation":[],"deletedLinkedUserRelation":[],"externalAppSettings":null,"fireBaseAuthToken":null,"flagEmailAddressHasChanged":false,"formMode":null,"globalBaremId":null,"hasAccessToAzuneedPortal":false,"hasBeenDeleted":false,"id":{"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idUser":"25e2925b-90d4-4f02-afc0-822192fcdfa3","parentclass":"com.azuneed.backoffice.appentity.user.UserBean","stringOf":"2697b4b7-ea54-4cac-a57a-bce57ac146a6[]25e2925b-90d4-4f02-afc0-822192fcdfa3"},"idCompany":"2697b4b7-ea54-4cac-a57a-bce57ac146a6","idUser":"25e2925b-90d4-4f02-afc0-822192fcdfa3","idUserProfile":null,"internalCode":null,"isActive":null,"isAnAdmin":null,"isArchived":null,"jenjiId":"553dbec7-7348-45a5-b99b-611d572f02ad","linkedManagerRelation":[],"linkedUserRelation":[],"loadIntro":false,"locale":null,"manageMealVoucher":true,"nationality":null,"paymentMethodId":null,"payrollResponsible":false,"qualification":null,"socialSecurityNumber":null,"tco":0.0,"thirdAccount":null,"userAddress":null,"userBirthDay":null,"userChildren":null,"userCompanyOrganizations":[],"userCreate":null,"userDatas":{"groups":[],"sysDataRights":{"mapProperty":{}}},"userDaysOffId":null,"userDelegations":null,"userDepartureDate":null,"userDirectories":null,"userEchelon":null,"userEmail":"test1705@test.com","userEmailTempKey":null,"userExtraWorkedDays":null,"userFirstName":"testRole","userFunction":null,"userGender":null,"userHiringDate":null,"userIbanItems":null,"userLastName":"TESTROLE","userLeaveVacationProfiles":null,"userLinkConnection":"https://rh.azuneed.com?userEmail=test1705@test.com&tempToken=gg982fRfR4uGNTPpfoBrNUCCsbJzCIlX89AsDU4JW8Uz_7Cm_D18sIbhy4M6S1t1h76rfAZp7BszMzc1MzI1Nw","userLocale":null,"userMaritalStatus":null,"userPayroll":null,"userPhones":[],"userPhoto":"resources/img/anonym_person_200x200.png","userPhotoFileBeanId":null,"userPhotoHasChanged":false,"userProducts":null,"userProfileLabel":null,"userPwd":null,"userSeniorityDate":null,"userSeniorityMonthToAdd":null,"userSerial":null,"userThirdApp":[],"userTimeZone":null,"userUpdate":null,"userWellBeingVotes":null,"userWidgetsPref":[],"userWorkCategory":null,"userWorkContracts":null,"userWorkedHours":null,"userWorkLevel":null,"workContractPackage":null,"workContractPackageNbUnit":null}]],"status":200,"totalCount":4}
  constructor(public userService: UserService,
              private service: AuthentificationService,
              private utilsService: UtilsService, 
              public dialog: MatDialog,
              private translateService: TranslateService,
              private filterService:FilterService
              ) {
    this.currentUser = this.service.currentUserValue;
    //this.filterService.setinitializeFilterSubject({'idCompany':[this.currentUser.user.userBean.id.idCompany]})
    this.filterAnnuaire = this.filterService.getInitFilter()
    // get employee
    this.getData()
    //this.getFilterContent();
  }
  ngOnInit(): void {

  }
  getData(){
    this.isLoading = true;
    this.employees = this.dataFake.message[0]
    this.employees.length = this.dataFake.totalCount;
    this.dataSource.data = this.employees
    this.isLoading = false;
    /*this.userService.getUsers(this.filterAnnuaire).subscribe((data: any) =>{
      this.employees = data.message[0]
      this.employees.length = data.totalCount;
      this.dataSource.data = this.employees
      this.isLoading = false;
    })*/
  }
  refresh(){
    this.isLoading = true;
    this.employees = this.dataFake.message[0]
    this.employees.length = this.dataFake.totalCount;
    this.dataSource.data = this.employees
    this.isLoading = false;
    /*this.userService.getUsers(this.filterAnnuaire).subscribe((data: any) =>{
      this.employees = data.message[0]
      this.employees.length = data.totalCount;
      this.dataSource.data = this.employees
      this.isLoading = false;
    })*/
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = 'Element par pages.';
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filterAnnuaire['searchInput'] =[filterValue.trim().toLowerCase()]
    this.employees = this.dataFake.message[0]
    this.employees.length = this.dataFake.totalCount;
    this.dataSource.data = this.employees
    this.isLoading = false;
   /* this.userService.getUsers(this.filterAnnuaire).subscribe((data: any) =>{
      this.employees = data.message[0]
      this.employees.length = data.totalCount;
      this.dataSource.data = this.employees
      this.isLoading = false;
    })*/

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onPageChanged(event: any) {
    const pageIndex = event.pageIndex;
    const pageSize = event.pageSize;
    const previousIndex = event.previousPageIndex;
    this.previousSize = pageSize * pageIndex;
    this.getNextData(this.previousSize, (pageIndex).toString(), pageSize.toString());
  }
  getNextData(currentSize: any, offset: any, limit: any) {
    this.isLoading = true;
    this.filterAnnuaire.limit = [limit]
    this.filterAnnuaire.offset = [currentSize]
    this.employees.length = currentSize;
    this.employees.push(...this.dataFake.message[0]);
    this.employees.length = this.dataFake.totalCount;
    this.dataSource = new MatTableDataSource<any>(this.employees);
    this.dataSource._updateChangeSubscription();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.isLoading = false;
    /*this.userService.getUsers(this.filterAnnuaire).subscribe((response: any) =>{
      this.employees.length = currentSize;
      this.employees.push(...response.message[0]);
      this.employees.length = response.totalCount;
      this.dataSource = new MatTableDataSource<any>(this.employees);
      this.dataSource._updateChangeSubscription();
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.isLoading = false;
    });*/
  }
  exportPdf(){
    this.isLoading= true;
    this.userService.exportPDF(this.filterAnnuaire).subscribe(data =>{
      this.userService.__exportData(data)
      this.isLoading= false;
    })

  }
  exportXLS(){
    this.isLoading= true;
    this.userService.exportXLS(this.filterAnnuaire).subscribe(data =>{
      this.userService.__exportData(data)
      this.isLoading= false;
    })
  }
  deleteEmployee(idUser : any){
    this.isLoading= true;
    this.userService.deleteUser(encodeURIComponent(idUser)).subscribe( {
      next: (data: any) => {
        this.utilsService.showSnackBar(`${data.message[0]}`, 'info-snack-bar');
        this.getData();
        this.isLoading= false;
      },
      error:(data: any) => {
        this.utilsService.showSnackBar(`${data.message}`, 'error-snack-bar');
        this.isLoading= false;
      }
    })
  }

  openDialog(value: any): void {
    const traductions = zip(
        this.translateService.get('msg.user.isDeleted.dialog.title'),
        this.translateService.get('msg.user.isDeleted.dialog.btnDelete'),
    );
    traductions.subscribe(translate =>{
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '500px',
        data: {type: "Confirmation",title: translate[0], value: translate[1], display: true, module:'directory'}

      });
      dialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed');
        if (result) {
          this.deleteEmployee(value);
        }
      });
    });

  }
  getFilterContent(){
    this.userService.getFilterContent(this.filterAnnuaire).subscribe((data: any) =>{
      this.contentFilter = data.message[0]
    })
  }
  openFilter() {
    const dialogRef = this.dialog.open(DynamicFormComponent, {
      data: this.contentFilter.message[0],
      direction: localStorage.getItem("isRtl") === "true" ? 'rtl' : 'ltr',
    });
    dialogRef.componentInstance.submitClicked.subscribe(result => {
      console.log('Got the data!', result);
      this.filterAnnuaire = this.filterService.getInitFilter()
      this.getData()
    });
  }
  parseDate(date:any){
    return new Date(date)
  }
}