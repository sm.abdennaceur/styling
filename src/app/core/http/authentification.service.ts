import { Injectable } from '@angular/core';

import { LanguageService } from "~core/http/language.service";
import { BaseService } from "./base.service";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { UserAuthentication } from "../models/UserAuthentication";
import { map } from "rxjs/operators";
import { serviceType, rights, modules } from '../helpers/app-constants';
import * as moment from 'moment';
import { ProfilePermission } from '~core/models/ProfilePermission.model';
import { ConfigService } from '~app/config/config.service';


@Injectable({
  providedIn: 'root'
})
export class AuthentificationService extends BaseService {
  user = new UserAuthentication();
  public currentUserSubject: BehaviorSubject<UserAuthentication> = new BehaviorSubject<UserAuthentication>({});
  public currentUser$ = this.currentUserSubject.asObservable();

  public resetPasswordObservable = new BehaviorSubject(false);

  private PermissionSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public Permission$ = this.PermissionSubject.asObservable();

  // @ts-ignore
  constructor(public http: HttpClient, public languageService: LanguageService,
              private configService: ConfigService) {
    super(http);
    const connectedUser = localStorage.getItem('connectedUser');
    this.setCurrentUser(connectedUser == null ? null : JSON.parse(connectedUser));

    const permission = localStorage.getItem('MapPermission');
    this.setProfilePermission(permission == null ? [] : JSON.parse(permission));
  }

  getProfilePermission() {
    // @ts-ignore
    return this.PermissionSubject.value != null ? this.PermissionSubject.value : JSON.parse(localStorage.getItem('MapPermission'));
  }

  setCurrentUser(data: any) {
    if (data) {
      this.user.token = data[0];
      this.user.user = data[1];
      this.currentUserSubject.next(this.user);
      localStorage.setItem('connectedUser', JSON.stringify(data));
      localStorage.setItem('userSession', JSON.stringify(this.user));
    }
  }

  setProfilePermission(permission: ProfilePermission[]) {
    this.PermissionSubject.next([...permission])
    localStorage.setItem('MapPermission', JSON.stringify(permission));
  }
  public get currentUserValue(): UserAuthentication {
    // @ts-ignore
    return this.currentUserSubject.value != null ? this.currentUserSubject.value : JSON.parse(localStorage.getItem('connectedUser'))
  }

  isAuth() {
    return this.currentUserValue != null;
  }

  isAbleTo() {
    const appSession = this.currentUserSubject.value
    return appSession ? appSession?.user?.userBean?.isAnAdmin == true : false;
  }

  login(username: string, password: string, connected: string) {
    return this.create('logon/auth', null, { "x-auth-user": username, "x-auth-key": password, "x-auth-stay-connected": connected ? 'true' : 'false' })
      .pipe(map((data: any) => {
        // login successful if there's a jwt token in the response
        if (data && data.code === 2000) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          this.setCurrentUser(data.message)
          this.initProfileMenu();

        }
        return data;
      }));
  }

  logout() {
    localStorage.removeItem('connectedUser');
    localStorage.removeItem('productAssociatedService');
    localStorage.removeItem('MapPermission');
    localStorage.removeItem('userSession');
    // @ts-ignore
    this.currentUserSubject.next(null);
    // @ts-ignore
    this.PermissionSubject.next(null);
    return;
  }

  resetPwdRequest(data: any) {
    return this.getData(`pwdUtil/updatePwdProcess/init?email=${data}&locale=${this.languageService.getCurLanguage()}`)
      .pipe(map(data => {
        return data;
      }));
  }

  applyNewPwdStateLess(concernedEmail: string, tempPwdKey: string, newPwd1: string, newPwd2: string) {
    return this.getData(`pwdUtil/updatePwdProcess/applyNewPwd/stateLess?tempPwdKey=${tempPwdKey}&concernedEmail=${concernedEmail}&locale=${this.languageService.getCurLanguage()}&newPwd1=${newPwd1}&newPwd2=${newPwd2}`)
      .pipe(map(data => {
        return data;
      }));
  }


  initProfileMenu() {
    this.getData('serviceUtil/getProductAssociationForEachService').subscribe((data: any) => {
      localStorage.setItem('productAssociatedService', JSON.stringify(data.message[0]));
      let permissionArry: ProfilePermission[] = this.getProfilePermission();
      permissionArry = []
      permissionArry.push({ 'name': serviceType.TimesheetServiceDef, 'value': this.isServiceASuscribedModule(serviceType.TimesheetServiceDef) });
      permissionArry.push({ 'name': serviceType.UserBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.UserBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.CompanyBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.CompanyBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.ConnectorBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.ConnectorBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.HourProfileBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.HourProfileBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.ServiceBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.ServiceBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.UserProfileBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.UserProfileBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.DaysOffBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.DaysOffBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.LeaveVacationTypeBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.LeaveVacationTypeBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.LeaveVacationProfileBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.LeaveVacationProfileBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.LeaveVacationRightBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.LeaveVacationRightBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.LeaveRequestBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.LeaveRequestBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.LeaveRequestBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.ChatBotBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.PayrollBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.PayrollBeanServiceDef) });

      permissionArry.push({ 'name': serviceType.WellBeingBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.WellBeingBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.AnalyticAxeServiceDef, 'value': this.isServiceASuscribedModule(serviceType.AnalyticAxeServiceDef) });
      permissionArry.push({ 'name': serviceType.TimesheetAnalysisServiceDef, 'value': this.isServiceASuscribedModule(serviceType.TimesheetAnalysisServiceDef) });
      permissionArry.push({ 'name': serviceType.KilometricBaremBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.KilometricBaremBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.VatBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.VatBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.RefundBaremBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.RefundBaremBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.ExpensesTypeServiceDef, 'value': this.isServiceASuscribedModule(serviceType.ExpensesTypeServiceDef) });
      permissionArry.push({ 'name': serviceType.UserDirectoryBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.UserDirectoryBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.UserDirectoryAdminBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.UserDirectoryAdminBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.DataUserChangeWfBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.DataUserChangeWfBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.UserDataEventBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.UserDataEventBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.DataGridBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.DataGridBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.DataGridModelBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.DataGridModelBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.communicationServiceDef, 'value': this.isServiceASuscribedModule(serviceType.communicationServiceDef) });
      permissionArry.push({ 'name': serviceType.formBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.formBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.campaignBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.campaignBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.interviewBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.interviewBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.goalBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.goalBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.notificationAdminServiceDef, 'value': this.isServiceASuscribedModule(serviceType.notificationAdminServiceDef) });

      permissionArry.push({ 'name': serviceType.isUserDirectoryASuscribedModule, 'permissionStatus': this.isAsuscribedModuleOrOption(modules.USER_DIRECTORY) });
      permissionArry.push({ 'name': serviceType.isInterviewASuscribedModule, 'permissionStatus': this.isAsuscribedModuleOrOption(modules.INTERVIEW) });
      permissionArry.push({ 'name': serviceType.isTimesheetASuscribedModule, 'permissionStatus': this.isAsuscribedModuleOrOption(modules.TIME_SHEET) });
      permissionArry.push({ 'name': serviceType.isLeaveRequestASuscribedModule, 'permissionStatus': this.isAsuscribedModuleOrOption(modules.LEAVE_REQUEST) });

      permissionArry.push({ 'name': serviceType.expenseAdminServiceDef, 'value': this.isServiceASuscribedModule(serviceType.expenseAdminServiceDef) });
      permissionArry.push({ 'name': serviceType.expenseBeanServiceDef, 'value': this.isServiceASuscribedModule(serviceType.expenseBeanServiceDef) });
      permissionArry.push({ 'name': serviceType.expenseAnalysisServiceDef, 'value': this.isServiceASuscribedModule(serviceType.expenseAnalysisServiceDef) });
      permissionArry.push({ 'name': serviceType.vehicleServiceDef, 'value': this.isServiceASuscribedModule(serviceType.vehicleServiceDef) });


      this.setProfilePermission(permissionArry);
    })
  }

  isServiceASuscribedModule(serviceType: string) {
    // @ts-ignore
    let productAssociatedService = JSON.parse(localStorage.getItem('productAssociatedService'))
    let objResult = { isActive: false, isDisabled: true, isASuscribedModule: false, validTill: '', activateDate: '', permissionCode: '' };
    let suscribedModulesAndOptions = this.currentUserValue.user?.suscribedModulesAndOptions;
    let product = productAssociatedService.mapProperty[serviceType];

    if (!product) {
      objResult.isActive = true;
      objResult.isDisabled = false;
    }

    if (product) {
      var productTab = product.split(';');
      for (var i = 0; i < productTab.length; i++) {
        var pName = productTab[i];
        if (suscribedModulesAndOptions.mapProperty[pName]) {
          if (suscribedModulesAndOptions.mapProperty[pName].isActive == true) {
            objResult.isActive = true;
            objResult.isASuscribedModule = true;

            // If valid till is defined
            if (suscribedModulesAndOptions.mapProperty[pName].validTill) {
              var m = moment(suscribedModulesAndOptions.mapProperty[pName].validTill, 'YYYY-MM-DD');
              var activateDate = moment(suscribedModulesAndOptions.mapProperty[pName].activateDate, 'YYYY-MM-DD');
              // @ts-ignore
              if (m.isBefore(moment(), 'YYYY-MM-DD')) {
                objResult.isDisabled = true;
                objResult.validTill = m.format('YYYY-MM-DD');
                //objResult.validMessage = translateFilter('msg.general.modulesoroptions.expired', {value:objResult.validTill});
              }
              // @ts-ignore
              else if (activateDate.isAfter(moment(), 'YYYY-MM-DD')) {
                objResult.isDisabled = true;
                objResult.activateDate = activateDate.format('YYYY-MM-DD');
                //	objResult.validMessage = translateFilter('msg.general.modulesoroptions.activate', {value:objResult.activateDate});
              }
              else
                objResult.isDisabled = false;
            }
            // If no valid till, don't disable the link
            else
              objResult.isDisabled = false;
          }
        }
      }
    }

    objResult.permissionCode = this.getForAGivenServiceAGivenParamValue(serviceType, rights.PARAM_ALLOW_DISPLAY)
    objResult.isActive = objResult.isActive == true ? this.getForAGivenServiceAGivenParamValue(serviceType, rights.PARAM_ALLOW_DISPLAY) > 0 : false;
    return objResult;
  }

  getForAGivenServiceAGivenParamValue(serviceType: any, idParam: any) {
    let userProfileMenu = this.currentUserValue?.user?.userProfileMenu;
    let result = null
    if (userProfileMenu && userProfileMenu.serviceParamMap.mapProperty[serviceType]) {
      result = userProfileMenu.serviceParamMap.mapProperty[serviceType].mapProperty[idParam].paramValue;
    }
    return result
  }

  isAsuscribedModuleOrOption(moduleOrOption: string) {
    let SuscribedModulesAndOptions = this.currentUserValue.user?.suscribedModulesAndOptions
    var m = moment(SuscribedModulesAndOptions.mapProperty[moduleOrOption]?.validTill, 'YYYY-MM-DD');
    var rawDate = SuscribedModulesAndOptions.mapProperty[moduleOrOption]?.validTill;
    var moduleActivateDate = moment(SuscribedModulesAndOptions.mapProperty[moduleOrOption]?.activateDate, 'YYYY-MM-DD');
    var ActivateRawDate = SuscribedModulesAndOptions.mapProperty[moduleOrOption]?.activateDate;
    return SuscribedModulesAndOptions.mapProperty[moduleOrOption]?.isActive == true && ((m.isSameOrAfter(moment().format('YYYY-MM-DD')) && moduleActivateDate.isSameOrBefore(moment().format('YYYY-MM-DD'))) || (moduleActivateDate.isSameOrBefore(moment().format('YYYY-MM-DD')) && !rawDate) || (m.isSameOrAfter(moment().format('YYYY-MM-DD')) && !ActivateRawDate) || (!rawDate && !ActivateRawDate));
  }

  hasAnyServicesDirect(services: string[]): boolean {
    let result = false
    for (let i = 0; i < services.length; i++) {
      let element = this.getProfilePermission().find((permission: any) => { return permission.name === services[i] })
      if (element) {
        result = element.value.isActive
      }
    }
    return result;
  }
}
