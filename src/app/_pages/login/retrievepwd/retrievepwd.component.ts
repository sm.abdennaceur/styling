import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {environment} from "~environments/environment";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthentificationService} from "~core/http/authentification.service";
import {UtilsService} from "~core/helpers/utils.service";

@Component({
  selector: 'app-retrievepwd',
  templateUrl: './retrievepwd.component.html',
  styleUrls: ['./retrievepwd.component.scss']
})
export class RetrievepwdComponent implements OnInit {
  env = environment
  retriveForm: FormGroup;
  showDetails: boolean;
  valid: any
  loading = false;
  tempPwdKey = '';
  concernedEmail ='';
  showPwd = true;


  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder,
              private service: AuthentificationService,
              private utilsService: UtilsService) { }

  ngOnInit(): void {
    this.showDetails = true
    this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.tempPwdKey && params.concernedEmail) {
        this.tempPwdKey = params.tempPwdKey;
        this.concernedEmail = params.concernedEmail;
      } else {
        this.router.navigate(['login']);
      }
    });
    this.retriveForm = this.formBuilder.group({
      password: ['', Validators.required],
      confirm_password: ['', [Validators.required]]
    },{
      validator: this.confirmedValidator('password', 'confirm_password')
    });
  }
  hasError(controlName: string, errorName: string) {
    return this.retriveForm.controls[controlName].hasError(errorName);
  }
  onConfirm() {
    this.service.applyNewPwdStateLess(this.concernedEmail,this.tempPwdKey, this.retriveForm.controls.password.value, this.retriveForm.controls.confirm_password.value).subscribe({
      next: (data: any) => {
        this.utilsService.showSnackBar(`${data.message[0]}`, 'info-snack-bar');
        if(data.code === 2000){
          const here = this;
          setTimeout(function(){
            here.router.navigate(['login']);
          }, 1000);
        }
        //
      },
      error: (err) => {
        this.utilsService.showSnackBar(`${err.message[0]}`, 'error-snack-bar');
      }
    })
  }

  onNoClick() {
    this.router.navigate(['login']);
  }

  confirmedValidator(controlName: string, matchingControlName: string){
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ confirmedValidator: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }
}
