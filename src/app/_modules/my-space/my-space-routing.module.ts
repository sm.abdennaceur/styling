import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MySpacePage } from './my-space.page';
import {EmployeeComponent} from "~app/_modules/my-space/employee/employee.component";

const routes: Routes = [
  { path: '', component: MySpacePage,  data: {title: 'msg.menu.mySpace'} },
  { path: 'user', component: EmployeeComponent,  data: {title: 'msg.menu.user'} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MySpaceRoutingModule { }
