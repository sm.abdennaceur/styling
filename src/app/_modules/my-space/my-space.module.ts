import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MySpaceRoutingModule } from './my-space-routing.module';
import { MaterialModule } from 'src/app/material/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MySpacePage } from './my-space.page';
import { EmployeeComponent } from './employee/employee.component';
import {FlexLayoutModule} from "@angular/flex-layout";


@NgModule({
  declarations: [
    MySpacePage,
    EmployeeComponent
  ],
  imports: [
    CommonModule,
    MySpaceRoutingModule,
    MaterialModule,
    SharedModule,
    FlexLayoutModule
  ]
})
export class MySpaceModule { }
