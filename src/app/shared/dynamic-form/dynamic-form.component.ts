import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { FilterItem } from '~core/helpers/app-constants';
import { FilterService } from '~core/service/filter.service';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})

export class DynamicFormComponent implements OnInit {
  @Output() submitClicked = new EventEmitter<any>();
  public myForm: FormGroup = this.fb.group({});
  constructor(
    private service:FilterService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DynamicFormComponent>,
    @Inject(MAT_DIALOG_DATA) public jsonFormData: any,
  ) {
    this.createForm(this.jsonFormData)
  }

  ngOnInit(): void { }

  createForm(controls: any[]) {
    const group: any = {};
    controls.forEach((item, index) => {
      group[item.filterType + '_' + index] = new FormControl('' || '');
    })
    this.myForm = new FormGroup(group);
    let oldFilter = this.service.currentFilterValue
    console.log('oldFilter :',oldFilter)
    if(oldFilter){
      for (const [key, value] of Object.entries(oldFilter)) {
        this.myForm.controls[key].setValue(value);
      }  
  }
  
  }

  onSubmit() {
    let result: any = {
      idCompany: [],
      idCompanyOrganization: [],
      idUser: [],
      appRole: []
    }
    
    for (const [key, value] of Object.entries(this.myForm.value)) {
      let newKey = this.getObjKey(key.split('_')[0]) as string
      if (value) {
        if (newKey === 'idCompanyOrganization') {
          result[newKey].push(value)
        } else {
          result[newKey] = value
        }
      }
    }
    this.service.updateFilter(this.myForm.value)
    this.service.setinitializeFilterSubject(result)
    this.submitClicked.emit(this.service.getInitFilter())
    this.onNoClick();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onChange(e: any) {
    e.target.value = ""
  }

  getObjKey(value: string) {
    //@ts-ignore
    return Object.keys(FilterItem).find(key => FilterItem[key] === value);
  }

}

