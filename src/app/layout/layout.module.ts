import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LeftbarComponent } from './leftbar/leftbar.component';
import { HeaderComponent } from './header/header.component';
import { FlexLayoutModule } from '@angular/flex-layout';

import { LayoutComponent } from './layout.component';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';
import { SidebarModule } from 'ng-sidebar';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ClickOutsideModule} from "ng-click-outside";
import {CoreModule} from "~core/core.module";
import { SharedModule } from '~shared/shared.module';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { MatTabsModule } from '@angular/material/tabs';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
  declarations: [
    LayoutComponent,
    SidebarComponent,
    LeftbarComponent,
    HeaderComponent, 
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    MaterialModule,
    SidebarModule,
    TranslateModule,
    ClickOutsideModule,
    CoreModule,
    SharedModule,
    NgbModule,
    MatTabsModule,
    PerfectScrollbarModule,
  ],
  exports:[
    SidebarComponent,
    LeftbarComponent,
    HeaderComponent
  ]
})
export class LayoutModule { }
