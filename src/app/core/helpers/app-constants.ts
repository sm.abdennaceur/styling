export class serviceType {
    public static UserBeanServiceDef = "user.UserBeanServiceDef";
    public static CompanyBeanServiceDef = "company.CompanyBeanServiceDef";
    public static ConnectorBeanServiceDef = "connector.ConnectorModelBeanServiceDef";
    public static HourProfileBeanServiceDef = "hourprofile.HourProfileBeanServiceDef";
    public static ServiceBeanServiceDef = "service.ServiceBeanServiceDef";
    public static UserProfileBeanServiceDef = "userprofile.UserProfileBeanServiceDef";
    public static DaysOffBeanServiceDef = "daysoff.DaysOffBeanServiceDef";
    public static LeaveVacationTypeBeanServiceDef = "leavevacation.leavevacationtype.LeaveVacationTypeBeanServiceDef";
    public static LeaveVacationProfileBeanServiceDef = "leavevacation.leavevacationprofile.LeaveVacationProfileBeanServiceDef";
    public static LeaveVacationRightBeanServiceDef = "leavevacation.leavevacationright.LeaveVacationRightBeanServiceDef";
    public static LeaveRequestBeanServiceDef = "leavevacation.leaverequest.LeaveRequestBeanServiceDef";
    public static ChatBotBeanServiceDef = "chatbot.ChatBotBeanServiceDef";
    public static UserDirectoryBeanServiceDef = "userdirectory.UserDirectoryBeanServiceDef";
    public static UserDirectoryAdminBeanServiceDef = "datagroup.UserDirectoryAdminBeanServiceDef";
    public static DataUserChangeWfBeanServiceDef = "dataworkflow.DataUserChangeWfBeanServiceDef";
    public static PayrollBeanServiceDef = "payroll.PayrollServiceDef";
    public static WellBeingBeanServiceDef = "wellbeing.WellBeingServiceDef";
    public static AnalyticAxeServiceDef = "analytic.AnalyticAxeBeanServiceDef";
    public static TimesheetServiceDef = "timesheet.TimesheetBeanServiceDef";
    public static TimesheetAnalysisServiceDef = "timesheet.analysis.TimesheetAnalysisBeanServiceDef";
    public static KilometricBaremBeanServiceDef = "expenses.kilometricbarem.KilometricBaremBeanServiceDef";
    public static VatBeanServiceDef = "expenses.vat.VatBeanServiceDef";
    public static RefundBaremBeanServiceDef = "expenses.refundbarem.RefundBaremBeanServiceDef";
    public static ExpensesTypeServiceDef = "expenses.expensestype.ExpensesTypeBeanServiceDef";
    public static UserDataEventBeanServiceDef = "userdataevents.UserDataEventBeanServiceDef";
    public static DataGridBeanServiceDef = "datagrid.DataGridBeanServiceDef";
    public static DataGridModelBeanServiceDef = "datagrid.DataGridModelBeanServiceDef";
    public static communicationServiceDef = "communication.CommunicationBeanServiceDef";
    public static formBeanServiceDef = "interviews.form.FormBeanServiceDef";
    public static campaignBeanServiceDef = "interviews.campaign.CampaignBeanServiceDef";
    public static interviewBeanServiceDef = "interviews.interview.InterviewBeanServiceDef";
    public static goalBeanServiceDef = "goals.goal.GoalBeanServiceDef";
    public static expenseAdminServiceDef = "expenses.ExpenseAdminServiceDef";
    public static expenseBeanServiceDef = "expenses.ExpenseBeanServiceDef";
    public static expenseAnalysisServiceDef = "expenses.ExpenseAnalysisServiceDef";
    public static vehicleServiceDef = "vehicle.VehicleServiceDef"
    public static notificationAdminServiceDef = "notifications.NotificationAdminServiceDef";
    public static isUserDirectoryASuscribedModule = "isUserDirectoryASuscribedModule";
    public static isInterviewASuscribedModule = "isInterviewASuscribedModule";
    public static isTimesheetASuscribedModule = "isTimesheetASuscribedModule";
    public static isLeaveRequestASuscribedModule = "isLeaveRequestASuscribedModule"
}

export class rights {
    public static PARAM_ALLOW_CREATE = "AllowCreate";
    public static PARAM_ALLOW_FORCE_PWD = "AllowToForcePassword";
    public static PARAM_ALLOW_DELETE = "AllowDelete";
    public static PARAM_ALLOW_EDIT = "AllowEdit";
    public static PARAM_ALLOW_ARCHIVE = "AllowArchive";
    public static PARAM_ALLOW_DISPLAY = "AllowDisplay";
    public static PARAM_ALLOW_EXPORT = "AllowGenerateExport";
    public static PARAM_ALLOW_GENERATE_EXPORT = "AllowGenerateExport";
    public static PARAM_COMPANY_VISIBILITY = "CompanyVisibility";
    public static PARAM_LR_PLANNING_VISIBILITY = "PlanningVisibility";
    public static PARAM_SEE_CRITICAL_INFO = "AllowSeeCriticalInfo";
    public static PARAM_ALLOW_ROLLBACK_PAYROLL = "AllowRollBackPayroll";
    public static PARAM_ALLOW_TO_CONNECT_WITH_OTHER_ACCOUNT = "AllowToConnectWithOtherAccount";
    public static PARAM_ALLOW_TO_MANAGE_OVERTIME = "AllowToManageOverTime";
    public static PARAM_TYPE_FILL = "TypeFill";
    public static PARAM_DISPLAY_SUBITEMS = "DisplaySubItems";
    public static PARAM_DISPLAY_EVENT = "DisplayEvent";
    public static PARAM_ALLOW_USERID_DISPLAY = "AllowUserIdDisplay";
    public static PARAM_ALLOW_USE_WF = "allowUseWf";
    public static VAC_REQ_ALLOW_UPDATE_LEAVE_TYPE = "AllowUpdateLeaveType";
    public static PARAM_ALLOW_REVOKE_POST_VALIDATE = "ParamAllowRevokePostValidate";
}

export class modules {
    public static ALL = 'all';
    public static LEAVE_REQUEST = "leave_request";
    public static TIME_SHEET = 'time_sheet';
    public static API = 'api';
    public static USER_DIRECTORY = 'user_directory';
    public static INTERVIEW = 'interviews';
    public static EXPENSES = 'expenses'

}
export const FilterItem ={
    idCompany:'CompanyBean',
    idCompanyOrganization :'CompanyOrganization',
    idUser:'UserBean',
    appRole:'appRole'
}
export class langConstant {
    public static AR = 'ar';
    public static FR = 'fr';
    public static EN = 'en';
}

export class COMPANY_SYS_ID {
    public static COMPANY_SYS_ID = 'sys_cny_id';
}

export interface Permission {
    timesheetServiceDefAccess?: any;
    userBeanServiceDefAccess?: any;
    companyBeanServiceDefAccess?: any;
    connectorBeanServiceDefAccess?: any;
    hourProfileBeanServiceDefAccess?: any;
    serviceBeanServiceDefAccess?: any;
    userProfileBeanServiceDefAccess?: any;
    daysOffBeanServiceDefAccess?: any;
    leaveVacationTypeBeanServiceDefAccess?: any;
    leaveVacationProfileBeanServiceDefAccess?: any;
    leaveVacationRightBeanServiceDefAccess?: any;
    leaveRequestBeanServiceDefAccess?: any;
    chatBotBeanServiceDefAccess?: any;
    payrollBeanServiceDefAccess?: any;
    wellBeingBeanServiceDefAccess?: any;
    analyticAxeServiceDefAccess?: any;
    timesheetAnalysisServiceDefAccess?: any;
    kilometricBaremServiceDefAccess?: any;
    vatDefAccess?: any;
    refundBaremDefAccess?: any;
    expensesTypeDefAccess?: any;
    userDirectoryBeanServiceDefAccess?: any;
    UserDirectoryAdminBeanServiceDef?: any;
    DataUserChangeWfBeanServiceDef?: any;
    UserDataEventBeanServiceDef?: any;
    DataGridBeanServiceDef?: any;
    DataGridModelBeanServiceDef?: any;
    communicationServiceDef?: any;
    formBeanServiceDef?: any;
    campaignBeanServiceDef?: any;
    interviewBeanServiceDef?: any;
    goalBeanServiceDef?: any;
    expensesBeanServiceDef?: any;
    notificationAdminServiceDef?: any;
    isUserDirectoryASuscribedModule?: any;
    isInterviewASuscribedModule?: any;
    isTimesheetASuscribedModule?: any;
    isLeaveRequestASuscribedModule?: any;
    expenseAdminServiceDefAccess?: any;
    expenseBeanServiceDefAccess?: any;
    expenseAnalysisServiceDefAccess?: any;
    vehicleServiceDefAccess?: any;
}
export  interface  Filter {
    idCompany ?:[];
    idUser ?: [];
    idCompanyOrganization?: [];
    status?: [];
    analyticAxeItem?: [];
    userDirectoryTagId?: [];
    userWorkCategory?: [];
    limit?: [];
    offset?: [];
    planType?: [];
    companyType?: [];
    idCompanyPartner?: [];
    withoutUserDirectory?: [],
    searchInput?: [],
    filterType?: [],
    requestsNeedActionBy?: [],
    appRole?: [],
    idPeriod?: [],
    accountingStatus?: [],
    leaveVacationTypeBeanId?: [],
    idCategory?: [],
    formId?: [],
    campaignId?: [],
    idType?: []
}