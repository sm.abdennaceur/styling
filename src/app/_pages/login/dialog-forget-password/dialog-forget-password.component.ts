import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {environment} from "~environments/environment";

@Component({
  selector: 'app-dialog-forget-password',
  templateUrl: './dialog-forget-password.component.html',
  styleUrls: ['./dialog-forget-password.component.scss']
})
export class DialogForgetPasswordComponent implements OnInit {
  public forgetPwdForm: FormGroup;
  envName = environment.name
  constructor(public dialogRef: MatDialogRef<DialogForgetPasswordComponent>) { }

  ngOnInit(): void {
    this.forgetPwdForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),

    });
  }
  hasError(controlName: string, errorName: string) {
    return this.forgetPwdForm.controls[controlName].hasError(errorName);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  onConfirm(): void {
    this.dialogRef.close(this.forgetPwdForm.controls);
  }
}
