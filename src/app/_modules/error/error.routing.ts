import { Routes } from '@angular/router';
import { Error404Component } from './error404/error404.component';
import { Error500Component } from './error500/error500.component';



export const ErrorRoutes: Routes = [
   //  { path: '', component: Error500Component,  data: {title: 'MyCompany'} },
    {
      

        path: '',
        children: [
            {
                path: '500',
                component: Error500Component
            },
            {
                path: '404',
                component: Error404Component
            }
        ]
    }
];
