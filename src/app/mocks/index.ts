export const validUser = {
    email: 'abdennaceur.smari@keyrus.com',
    password: 'Wajdi@22849445',
    connected:false
};

export const blankUser = {
    email: '',
    password: '',
    connected:false
};

export const testUserData = [
    "2BB4C28A077E41325526EAE6831FDBE0",
    {
      "chatBotContext": null,
      "cnyBean": {
        "affectedConnectors": null,
        "allowExpenseWfMail": null,
        "apiKeys": null,
        "appIdPartner": null,
        "blockMobileAccess": false,
        "categories": null,
        "clientCrmCode": null,
        "cnyDaysOffId": null,
        "cnyLocale": null,
        "cnyLocaleLabel": null,
        "cnyName": null,
        "cnyTimeZone": "Europe/Paris",
        "cnyToken": null,
        "cnyType": "azuneed",
        "cnyTypeLabel": null,
        "companyActivationDate": null,
        "companyBillingRelations": null,
        "companyConstraints": null,
        "companyEndAccess": null,
        "companyItemList": null,
        "companyOrgCollection": null,
        "companyProductSubscription": null,
        "country": "FR",
        "currency": "EUR",
        "dataGridModelNotifications": null,
        "dataPermissionId": null,
        "dataWorkflowBeanId": null,
        "dateCreate": null,
        "dateUpdate": null,
        "disabledAssistanceTchat": false,
        "emailSupport": "",
        "employeesNumber": null,
        "expensePayrollCode": null,
        "externalAppSettings": null,
        "externalSbcpValues": null,
        "fixedMileageRate": null,
        "formMode": null,
        "globalBaremId": null,
        "hasBeenDeleted": false,
        "hourProfileId": null,
        "id": {
          "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
          "idUser": null,
          "parentclass": "com.azuneed.backoffice.appentity.company.CompanyBean",
          "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6"
        },
        "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
        "idUser": null,
        "internalCode": null,
        "isArchived": null,
        "isBotEnabled": false,
        "isCnyModel": null,
        "isMtoM": null,
        "jenjiId": null,
        "leaveVacationProfileBeanId": null,
        "locale": null,
        "logo": null,
        "logoFileBeanId": null,
        "logoHasChanged": false,
        "mileageRefundBarem": null,
        "partnerCode": null,
        "partnerCrmCode": null,
        "paymentMethodId": {
          "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
          "idPaymentMethod": "65c768d7-349f-0ec0-20f7-e7deade631c1",
          "idUser": null,
          "parentclass": null,
          "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]65c768d7-349f-0ec0-20f7-e7deade631c1"
        },
        "payrollSettings": null,
        "phoneSupport": "",
        "planType": "sbcp",
        "planTypeLabel": null,
        "referringEmail": null,
        "serialNumber": null,
        "serialNumber2": null,
        "tags": null,
        "theme": null,
        "tsWorkPackageOn": true,
        "userCreate": null,
        "userNameSupport": "",
        "userProfileDetail": null,
        "userProfileId": null,
        "userUpdate": null,
        "welcomeText": "Bienvenue sur Sage Espace Employés !\nVous êtes sur l'environnement de PREPROD.",
        "workflowBeanId": null
      },
      "consumedForEachServicePlan": {
        "mapProperty": {}
      },
      "idInstantiation": "f7407896-7f4e-4fa7-91b2-0ec81a67d227",
      "linkedCompanies": ",'2697b4b7-ea54-4cac-a57a-bce57ac146a6'",
      "linkedUserDirect": "",
      "linkedUserIndirect": "",
      "sessionLocale": "fr_FR",
      "sessionTimeZone": "Europe/Paris",
      "suscribedModulesAndOptions": {
        "mapProperty": {
          "user_directory": {
            "activateDate": null,
            "crmProductId": null,
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idProduct": "user_directory",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.company.CompanyProduct",
              "stringOf": null
            },
            "isActive": true,
            "nbConsumed": null,
            "nbUnit": 1,
            "product": {
              "detail": [
                {
                  "extraLongDetail": null,
                  "locale": "en_GB",
                  "longDetail": null,
                  "shortDetail": "Employee Directories"
                },
                {
                  "extraLongDetail": null,
                  "locale": "fr_FR",
                  "longDetail": null,
                  "shortDetail": "Dossier Salariés"
                }
              ],
              "id": {
                "idCompany": null,
                "idProduct": "user_directory",
                "idUser": null,
                "parentclass": "com.azuneed.backoffice.appentity.product.ProductBean",
                "stringOf": "user_directory"
              },
              "isActif": true,
              "productImageName": "ic_people_white_48px",
              "productType": "modules"
            },
            "usersList": null,
            "validTill": "2080-12-29"
          },
          "payroll_connection": {
            "activateDate": null,
            "crmProductId": null,
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idProduct": "payroll_connection",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.company.CompanyProduct",
              "stringOf": null
            },
            "isActive": true,
            "nbConsumed": null,
            "nbUnit": 1,
            "product": {
              "detail": [
                {
                  "extraLongDetail": null,
                  "locale": "en_GB",
                  "longDetail": null,
                  "shortDetail": "Payroll interface"
                },
                {
                  "extraLongDetail": null,
                  "locale": "fr_FR",
                  "longDetail": null,
                  "shortDetail": "Interface Paie"
                }
              ],
              "id": {
                "idCompany": null,
                "idProduct": "payroll_connection",
                "idUser": null,
                "parentclass": "com.azuneed.backoffice.appentity.product.ProductBean",
                "stringOf": "payroll_connection"
              },
              "isActif": true,
              "productImageName": "ic_euro_symbol_white_48px",
              "productType": "options"
            },
            "usersList": null,
            "validTill": "2080-12-29"
          },
          "leave_request": {
            "activateDate": null,
            "crmProductId": null,
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idProduct": "leave_request",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.company.CompanyProduct",
              "stringOf": null
            },
            "isActive": true,
            "nbConsumed": null,
            "nbUnit": 1,
            "product": {
              "detail": [
                {
                  "extraLongDetail": null,
                  "locale": "en_GB",
                  "longDetail": null,
                  "shortDetail": "Leave Requests"
                },
                {
                  "extraLongDetail": null,
                  "locale": "fr_FR",
                  "longDetail": null,
                  "shortDetail": "Congés et Absences"
                }
              ],
              "id": {
                "idCompany": null,
                "idProduct": "leave_request",
                "idUser": null,
                "parentclass": "com.azuneed.backoffice.appentity.product.ProductBean",
                "stringOf": "leave_request"
              },
              "isActif": true,
              "productImageName": "ic_beach_access_white_48px",
              "productType": "modules"
            },
            "usersList": null,
            "validTill": "2080-12-29"
          },
          "well_being": {
            "activateDate": null,
            "crmProductId": null,
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idProduct": "well_being",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.company.CompanyProduct",
              "stringOf": null
            },
            "isActive": true,
            "nbConsumed": null,
            "nbUnit": 1,
            "product": {
              "detail": [
                {
                  "extraLongDetail": null,
                  "locale": "en_GB",
                  "longDetail": null,
                  "shortDetail": "Well Being"
                },
                {
                  "extraLongDetail": null,
                  "locale": "fr_FR",
                  "longDetail": null,
                  "shortDetail": "Bien Être"
                }
              ],
              "id": {
                "idCompany": null,
                "idProduct": "well_being",
                "idUser": null,
                "parentclass": "com.azuneed.backoffice.appentity.product.ProductBean",
                "stringOf": "well_being"
              },
              "isActif": true,
              "productImageName": "ic_face_white_48px",
              "productType": "modules"
            },
            "usersList": null,
            "validTill": "2080-12-29"
          },
          "api": {
            "activateDate": null,
            "crmProductId": null,
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idProduct": "api",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.company.CompanyProduct",
              "stringOf": null
            },
            "isActive": true,
            "nbConsumed": null,
            "nbUnit": 1,
            "product": {
              "detail": [
                {
                  "extraLongDetail": null,
                  "locale": "en_GB",
                  "longDetail": null,
                  "shortDetail": "Api"
                },
                {
                  "extraLongDetail": null,
                  "locale": "fr_FR",
                  "longDetail": null,
                  "shortDetail": "Api"
                }
              ],
              "id": {
                "idCompany": null,
                "idProduct": "api",
                "idUser": null,
                "parentclass": "com.azuneed.backoffice.appentity.product.ProductBean",
                "stringOf": "api"
              },
              "isActif": true,
              "productImageName": "ic_settings_input_component_white_48px",
              "productType": "options"
            },
            "usersList": null,
            "validTill": "2080-12-29"
          },
          "disk_allocation": {
            "activateDate": null,
            "crmProductId": null,
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idProduct": "disk_allocation",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.company.CompanyProduct",
              "stringOf": null
            },
            "isActive": true,
            "nbConsumed": null,
            "nbUnit": 1,
            "product": {
              "detail": [
                {
                  "extraLongDetail": null,
                  "locale": "en_GB",
                  "longDetail": null,
                  "shortDetail": "Disk allocation"
                },
                {
                  "extraLongDetail": null,
                  "locale": "fr_FR",
                  "longDetail": null,
                  "shortDetail": "Espace disque"
                }
              ],
              "id": {
                "idCompany": null,
                "idProduct": "disk_allocation",
                "idUser": null,
                "parentclass": "com.azuneed.backoffice.appentity.product.ProductBean",
                "stringOf": "disk_allocation"
              },
              "isActif": true,
              "productImageName": "ic_storage_white_48px",
              "productType": "options"
            },
            "usersList": null,
            "validTill": "2080-12-29"
          },
          "payroll_api_connection": {
            "activateDate": null,
            "crmProductId": null,
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idProduct": "payroll_api_connection",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.company.CompanyProduct",
              "stringOf": null
            },
            "isActive": true,
            "nbConsumed": null,
            "nbUnit": 1,
            "product": {
              "detail": [
                {
                  "extraLongDetail": null,
                  "locale": "en_GB",
                  "longDetail": null,
                  "shortDetail": "Payroll API connection"
                },
                {
                  "extraLongDetail": null,
                  "locale": "fr_FR",
                  "longDetail": null,
                  "shortDetail": "Connecteur paie"
                }
              ],
              "id": {
                "idCompany": null,
                "idProduct": "payroll_api_connection",
                "idUser": null,
                "parentclass": "com.azuneed.backoffice.appentity.product.ProductBean",
                "stringOf": "payroll_api_connection"
              },
              "isActif": true,
              "productImageName": "ic_euro_symbol_white_48px",
              "productType": "options"
            },
            "usersList": null,
            "validTill": "2080-12-29"
          },
          "time_sheet": {
            "activateDate": null,
            "crmProductId": null,
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idProduct": "time_sheet",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.company.CompanyProduct",
              "stringOf": null
            },
            "isActive": true,
            "nbConsumed": null,
            "nbUnit": 1,
            "product": {
              "detail": [
                {
                  "extraLongDetail": null,
                  "locale": "en_GB",
                  "longDetail": null,
                  "shortDetail": "Timesheet"
                },
                {
                  "extraLongDetail": null,
                  "locale": "fr_FR",
                  "longDetail": null,
                  "shortDetail": "Temps et Activités"
                }
              ],
              "id": {
                "idCompany": null,
                "idProduct": "time_sheet",
                "idUser": null,
                "parentclass": "com.azuneed.backoffice.appentity.product.ProductBean",
                "stringOf": "time_sheet"
              },
              "isActif": true,
              "productImageName": "ic_timer_white_48px",
              "productType": "modules"
            },
            "usersList": null,
            "validTill": "2080-12-29"
          },
          "expenses": {
            "activateDate": null,
            "crmProductId": null,
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idProduct": "expenses",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.company.CompanyProduct",
              "stringOf": null
            },
            "isActive": true,
            "nbConsumed": null,
            "nbUnit": 1,
            "product": {
              "detail": [
                {
                  "extraLongDetail": null,
                  "locale": "en_GB",
                  "longDetail": null,
                  "shortDetail": "Expenses"
                },
                {
                  "extraLongDetail": null,
                  "locale": "fr_FR",
                  "longDetail": null,
                  "shortDetail": "Note de frais"
                }
              ],
              "id": {
                "idCompany": null,
                "idProduct": "expenses",
                "idUser": null,
                "parentclass": "com.azuneed.backoffice.appentity.product.ProductBean",
                "stringOf": "expenses"
              },
              "isActif": true,
              "productImageName": "ic_expense_logoa",
              "productType": "modules"
            },
            "usersList": null,
            "validTill": "2080-12-28"
          }
        }
      },
      "systemTimeZone": "Africa/Tunis",
      "userBean": {
        "anHrResponsible": true,
        "applyVisibilityWithNoUserDirectory": null,
        "appRole": ";hr;payroll;admin",
        "azuneedInfo": null,
        "bypassSync": null,
        "byPassWf": null,
        "companyDetail": null,
        "country": null,
        "currency": "EUR",
        "dataPermissionId": null,
        "dataValues": [],
        "dateCreate": null,
        "dateUpdate": null,
        "deletedLinkedManagerRelation": [],
        "deletedLinkedUserRelation": [],
        "externalAppSettings": null,
        "fireBaseAuthToken": null,
        "flagEmailAddressHasChanged": false,
        "formMode": null,
        "globalBaremId": null,
        "hasAccessToAzuneedPortal": false,
        "hasBeenDeleted": false,
        "id": {
          "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
          "idUser": "458c5405-40a2-4acd-97f0-02a0155921a0",
          "parentclass": "com.azuneed.backoffice.appentity.user.UserBean",
          "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]458c5405-40a2-4acd-97f0-02a0155921a0"
        },
        "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
        "idUser": "458c5405-40a2-4acd-97f0-02a0155921a0",
        "idUserProfile": null,
        "internalCode": null,
        "isActive": null,
        "isArchived": null,
        "jenjiId": "8ab06a2b-5637-48bb-9a86-04b8cbc2149b",
        "linkedManagerRelation": null,
        "linkedUserRelation": [],
        "loadIntro": false,
        "locale": null,
        "manageMealVoucher": true,
        "nationality": null,
        "paymentMethodId": null,
        "payrollResponsible": true,
        "qualification": null,
        "socialSecurityNumber": null,
        "tco": 0,
        "thirdAccount": null,
        "userAddress": null,
        "userBirthDay": null,
        "userChildren": null,
        "userCompanyOrganizations": null,
        "userCreate": null,
        "userDatas": {
          "groups": [],
          "sysDataRights": {
            "mapProperty": {}
          }
        },
        "userDaysOffId": null,
        "userDelegations": null,
        "userDepartureDate": null,
        "userDirectories": null,
        "userEchelon": null,
        "userEmail": "abdennaceur.smari@keyrus.com",
        "userEmailTempKey": null,
        "userExtraWorkedDays": null,
        "userFirstName": "abdennaceur",
        "userFunction": null,
        "userGender": null,
        "userHiringDate": null,
        "userIbanItems": null,
        "userLastName": "SMARI",
        "userLeaveVacationProfiles": null,
        "userLinkConnection": null,
        "userLocale": null,
        "userMaritalStatus": null,
        "userPayroll": null,
        "userPhones": null,
        "userPhoto": "resources/img/anonym_person_200x200.png",
        "userPhotoFileBeanId": null,
        "userPhotoHasChanged": false,
        "userProducts": null,
        "userProfileLabel": "Administrateur",
        "userPwd": null,
        "userSeniorityDate": null,
        "userSeniorityMonthToAdd": null,
        "userSerial": null,
        "userThirdApp": [],
        "userTimeZone": null,
        "userUpdate": null,
        "userWellBeingVotes": null,
        "userWidgetsPref": [],
        "userWorkCategory": null,
        "userWorkContracts": null,
        "userWorkedHours": null,
        "userWorkLevel": null,
        "workContractPackage": null,
        "workContractPackageNbUnit": null
      },
      "userProfileMenu": {
        "codeUserProfile": "Administrateur",
        "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
        "idUserProfile": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3",
        "serviceParamMap": {
          "mapProperty": {
            "daysoff.DaysOffBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "false"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                }
              }
            },
            "leavevacation.leavevacationprofile.LeaveVacationProfileBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "false"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                }
              }
            },
            "hourprofile.HourProfileBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "false"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                }
              }
            },
            "userdirectory.UserDirectoryBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                }
              }
            },
            "vehicle.VehicleServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                }
              }
            },
            "leavevacation.leaverequest.LeaveRequestBeanServiceDef": {
              "mapProperty": {
                "AllowUpdateUserExtraDays": {
                  "associatedBundleKey": "leaveRequest.param.def.planning.update.user.extra.days",
                  "associatedHelpBundleKey": "leaveRequest.param.def.planning.update.user.extra.days.help",
                  "idName": "AllowUpdateUserExtraDays",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowUpdateLeaveType": {
                  "associatedBundleKey": "leaveRequest.param.def.planning.update.leave.type",
                  "associatedHelpBundleKey": "leaveRequest.param.def.planning.update.leave.type.help",
                  "idName": "AllowUpdateLeaveType",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "true"
                },
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowSeeHiddenLeaveType": {
                  "associatedBundleKey": "leaveRequest.param.def.AllowSeeHiddenLeaveType.label",
                  "associatedHelpBundleKey": "leaveRequest.param.def.AllowSeeHiddenLeaveType.label.help",
                  "idName": "AllowSeeHiddenLeaveType",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "PlanningVisibility": {
                  "associatedBundleKey": "leaveRequest.param.def.planning.visibility.label",
                  "associatedHelpBundleKey": "leaveRequest.param.def.planning.visibility.label.help",
                  "idName": "PlanningVisibility",
                  "myNotion": false,
                  "otherListToDisplay": [],
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "3"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "DisplayEvent": {
                  "associatedBundleKey": "param.def.displayEmployeeEvent.label",
                  "associatedHelpBundleKey": "param.def.displayEmployeeEvent.label.help",
                  "idName": "DisplayEvent",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "DISPLAYPREF",
                  "paramValue": "false"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                }
              }
            },
            "leavevacation.leavevacationtype.LeaveVacationTypeBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "false"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                }
              }
            },
            "expenses.ExpenseAdminServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                }
              }
            },
            "user.UserBeanServiceDef": {
              "mapProperty": {
                "AllowToConnectWithOtherAccount": {
                  "associatedBundleKey": "param.def.connectToOtherUser.user",
                  "associatedHelpBundleKey": "param.def.connectToOtherUser.user.help",
                  "idName": "AllowToConnectWithOtherAccount",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowSeeOrganizational": {
                  "associatedBundleKey": "param.def.organizational.label.user",
                  "associatedHelpBundleKey": "param.def.seeCriticalInfo.label.help",
                  "idName": "AllowSeeOrganizational",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowSeeCriticalInfo": {
                  "associatedBundleKey": "param.def.seeCriticalInfo.label.user",
                  "associatedHelpBundleKey": "param.def.seeCriticalInfo.label.help",
                  "idName": "AllowSeeCriticalInfo",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "false"
                },
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowToForcePassword": {
                  "associatedBundleKey": "param.def.allowForcePassword.user",
                  "associatedHelpBundleKey": "param.def.allowForcePassword.user.help",
                  "idName": "AllowToForcePassword",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "true"
                },
                "AllowUserIdDisplay": {
                  "associatedBundleKey": "param.def.allowUserIdDisplay.user",
                  "associatedHelpBundleKey": "param.def.allowUserIdDisplay.user.help",
                  "idName": "AllowUserIdDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "DISPLAYPREF",
                  "paramValue": "false"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                }
              }
            },
            "payroll.PayrollServiceDef": {
              "mapProperty": {
                "AllowRollBackPayroll": {
                  "associatedBundleKey": "param.def.allowRollbackPayroll.label.payroll",
                  "associatedHelpBundleKey": "param.def.allowRollbackPayroll.label.payroll.help",
                  "idName": "AllowRollBackPayroll",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                }
              }
            },
            "dataworkflow.DataUserChangeWfBeanServiceDef": {
              "mapProperty": {
                "allowUseWf": {
                  "associatedBundleKey": "param.def.allowUseWf.dataWorkflow",
                  "associatedHelpBundleKey": "param.def.allowUseWf.dataWorkflow.help",
                  "idName": "allowUseWf",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "false"
                },
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                }
              }
            },
            "expenses.ExpenseAnalysisServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "0"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                }
              }
            },
            "userdataevents.UserDataEventBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                }
              }
            },
            "datagrid.DataGridModelBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                }
              }
            },
            "company.CompanyBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "false"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "0"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                }
              }
            },
            "datagrid.DataGridBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                }
              }
            },
            "notifications.NotificationAdminServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "10"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "10"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "0"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "10"
                }
              }
            },
            "leavevacation.leavevacationright.LeaveVacationRightBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                }
              }
            },
            "expenses.ExpenseBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "0"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                },
                "ParamAllowRevokePostValidate": {
                  "associatedBundleKey": "param.def.allowRevokePostValidate.expenses",
                  "associatedHelpBundleKey": "param.def.allowRevokePostValidate.expenses.help",
                  "idName": "ParamAllowRevokePostValidate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": true,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "13"
                }
              }
            },
            "datagroup.UserDirectoryAdminBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "true"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                }
              }
            },
            "userprofile.UserProfileBeanServiceDef": {
              "mapProperty": {
                "AllowEdit": {
                  "associatedBundleKey": "param.def.allowEdit.label",
                  "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                  "idName": "AllowEdit",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowGenerateExport": {
                  "associatedBundleKey": "param.def.allowGenerateExport.label",
                  "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                  "idName": "AllowGenerateExport",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "AUTHORIZATION",
                  "paramValue": "false"
                },
                "AllowDelete": {
                  "associatedBundleKey": "param.def.allowDelete.label",
                  "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                  "idName": "AllowDelete",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowCreate": {
                  "associatedBundleKey": "param.def.allowCreate.label",
                  "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                  "idName": "AllowCreate",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "CompanyVisibility": {
                  "associatedBundleKey": "param.def.companyVisibility.label",
                  "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                  "idName": "CompanyVisibility",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                },
                "AllowDisplay": {
                  "associatedBundleKey": "param.def.allowDisplay.label",
                  "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                  "idName": "AllowDisplay",
                  "myNotion": false,
                  "otherListToDisplay": null,
                  "paramFamilyType": "RIGHTS",
                  "paramValue": "1"
                }
              }
            }
          }
        },
        "userDirectoryTagRights": {
          "mapProperty": {
            "USER_WORKED_HOURS": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_HIRING_DATE": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_PAYROLL": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_WORK_CONTRACT": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_WORK_LEVEL": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_EMAIL": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "LINKED_USER_RELATION": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_CHILDREN": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_MEAL_VOUCHER": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_SENIORITY_DATE": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_SENIORITY_MONTH_TO_ADD": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_FUNCTION": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_WORK_CONTRACT_PACKAGE": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_WORK_CONTRACT_PACKAGE_NB_UNIT": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_PRODUCT": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "DAYS_OFF_CODE": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "THIRD_ACCOUNT": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_PROFILE_CODE": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_TCO": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_QUALIFICATION": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "BIRTHDAY": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_DEPARTURE_DATE": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_ECHELON": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_PAYROLL_SLIP": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_COMPANY_ORGANIZATION": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_APP_ROLE_CODE": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_FIRST_NAME": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_TIMEZONE": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_LOCALE": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_LEAVE_PROFILE": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_IBAN": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_SOCIAL_SECURITY_NUMBER": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "LINKED_USER_MANAGER": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_ADDRESS": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_MANAGE_MEAL_VOUCHER": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_WORK_CATEGORY": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_GENDER": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_NATIONALITY": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_PHONE": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_DATA_PERMISSION_CODE": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_PHOTO": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_MARITAL_STATUS": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_LAST_NAME": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            },
            "USER_SERIAL": {
              "displayRightToInt": 13,
              "editRightToInt": 13,
              "otherAllowDisplay": "3",
              "otherAllowEdit": "3",
              "ownerAllowDisplay": true,
              "ownerAllowEdit": true
            }
          }
        },
        "userProfileAccess": [
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]b49353c2-29cd-b8fb-d7ba-7db17c3b30e5",
              "userProfileAccessId": "b49353c2-29cd-b8fb-d7ba-7db17c3b30e5",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": false,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService23",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService23",
              "toString": "sys_cny_id[]idService23"
            },
            "serviceType": "userdirectory.UserDirectoryBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "User directory access"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Accès au dossier salarié"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]60a0a405-eff4-34a1-f125-2bf1696b7813",
              "userProfileAccessId": "60a0a405-eff4-34a1-f125-2bf1696b7813",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": false,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService27",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService27",
              "toString": "sys_cny_id[]idService27"
            },
            "serviceType": "datagrid.DataGridBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Variable payroll item grid access"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Accès aux grilles de saisies"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess26",
              "userProfileAccessId": "idUserProfileAccess26",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": true,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService26",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService26",
              "toString": "sys_cny_id[]idService26"
            },
            "serviceType": "userdataevents.UserDataEventBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Variable payroll item access"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Accès à la liste des éléments variables de paie"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]39c4815b-7f94-0b8c-ef12-1ee3c71074a0",
              "userProfileAccessId": "39c4815b-7f94-0b8c-ef12-1ee3c71074a0",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": false,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService28",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService28",
              "toString": "sys_cny_id[]idService28"
            },
            "serviceType": "datagrid.DataGridModelBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Variable payroll item grid model administration"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Administration des modèles de grilles de saisies"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess35",
              "userProfileAccessId": "idUserProfileAccess35",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": false,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService35",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService35",
              "toString": "sys_cny_id[]idService35"
            },
            "serviceType": "expenses.ExpenseAdminServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Administration of expense reports"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Administration des notes de frais"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess25",
              "userProfileAccessId": "idUserProfileAccess25",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": true,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService25",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService25",
              "toString": "sys_cny_id[]idService25"
            },
            "serviceType": "dataworkflow.DataUserChangeWfBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Data workflows administration"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Administration des workflow de données"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowUseWf.dataWorkflow",
                "associatedHelpBundleKey": "param.def.allowUseWf.dataWorkflow.help",
                "idName": "allowUseWf",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "false"
              },
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]263fb7d0-d5e8-4b5c-4fc3-bd101ce3e2b6",
              "userProfileAccessId": "263fb7d0-d5e8-4b5c-4fc3-bd101ce3e2b6",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": false,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService24",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService24",
              "toString": "sys_cny_id[]idService24"
            },
            "serviceType": "datagroup.UserDirectoryAdminBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "User directory administration"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Administration du dossier salarié"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]6610b1c6-7a64-bb91-b317-4bfe8c76b392",
              "userProfileAccessId": "6610b1c6-7a64-bb91-b317-4bfe8c76b392",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": false,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService37",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService37",
              "toString": "sys_cny_id[]idService37"
            },
            "serviceType": "expenses.ExpenseAnalysisServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Expense reports"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Analyse et rapprochement comptable"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "0"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess1",
              "userProfileAccessId": "idUserProfileAccess1",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": true,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService1",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService1",
              "toString": "sys_cny_id[]idService1"
            },
            "serviceType": "user.UserBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Employee directory"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Annuaire salariés"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.seeCriticalInfo.label.user",
                "associatedHelpBundleKey": "param.def.seeCriticalInfo.label.help",
                "idName": "AllowSeeCriticalInfo",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "false"
              },
              {
                "associatedBundleKey": "param.def.connectToOtherUser.user",
                "associatedHelpBundleKey": "param.def.connectToOtherUser.user.help",
                "idName": "AllowToConnectWithOtherAccount",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowUserIdDisplay.user",
                "associatedHelpBundleKey": "param.def.allowUserIdDisplay.user.help",
                "idName": "AllowUserIdDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "DISPLAYPREF",
                "paramValue": "false"
              },
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.organizational.label.user",
                "associatedHelpBundleKey": "param.def.seeCriticalInfo.label.help",
                "idName": "AllowSeeOrganizational",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowForcePassword.user",
                "associatedHelpBundleKey": "param.def.allowForcePassword.user.help",
                "idName": "AllowToForcePassword",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess10",
              "userProfileAccessId": "idUserProfileAccess10",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": true,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService10",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService10",
              "toString": "sys_cny_id[]idService10"
            },
            "serviceType": "leavevacation.leavevacationright.LeaveVacationRightBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Leave rights"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Compteurs de congés"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess11",
              "userProfileAccessId": "idUserProfileAccess11",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": true,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService11",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService11",
              "toString": "sys_cny_id[]idService11"
            },
            "serviceType": "leavevacation.leaverequest.LeaveRequestBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Leave requests and absences"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Demande de congés et d'absences"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "leaveRequest.param.def.AllowSeeHiddenLeaveType.label",
                "associatedHelpBundleKey": "leaveRequest.param.def.AllowSeeHiddenLeaveType.label.help",
                "idName": "AllowSeeHiddenLeaveType",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "leaveRequest.param.def.planning.update.user.extra.days",
                "associatedHelpBundleKey": "leaveRequest.param.def.planning.update.user.extra.days.help",
                "idName": "AllowUpdateUserExtraDays",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "leaveRequest.param.def.planning.update.leave.type",
                "associatedHelpBundleKey": "leaveRequest.param.def.planning.update.leave.type.help",
                "idName": "AllowUpdateLeaveType",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "leaveRequest.param.def.planning.visibility.label",
                "associatedHelpBundleKey": "leaveRequest.param.def.planning.visibility.label.help",
                "idName": "PlanningVisibility",
                "myNotion": false,
                "otherListToDisplay": [],
                "paramFamilyType": "RIGHTS",
                "paramValue": "3"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.displayEmployeeEvent.label",
                "associatedHelpBundleKey": "param.def.displayEmployeeEvent.label.help",
                "idName": "DisplayEvent",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "DISPLAYPREF",
                "paramValue": "false"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess38",
              "userProfileAccessId": "idUserProfileAccess38",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": true,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService38",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService38",
              "toString": "sys_cny_id[]idService38"
            },
            "serviceType": "notifications.NotificationAdminServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Handling user notifications"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Gestion des notifications utilisateurs"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "10"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "10"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "10"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "0"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess39",
              "userProfileAccessId": "idUserProfileAccess39",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": true,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService39",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService39",
              "toString": "sys_cny_id[]idService39"
            },
            "serviceType": "vehicle.VehicleServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Vehicle management"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Gestion des véhicules"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess13",
              "userProfileAccessId": "idUserProfileAccess13",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": true,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService13",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService13",
              "toString": "sys_cny_id[]idService13"
            },
            "serviceType": "payroll.PayrollServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Payroll datas generator"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Génération des données pour le logiciel de paye"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowRollbackPayroll.label.payroll",
                "associatedHelpBundleKey": "param.def.allowRollbackPayroll.label.payroll.help",
                "idName": "AllowRollBackPayroll",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess6",
              "userProfileAccessId": "idUserProfileAccess6",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": false,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService6",
              "toString": "sys_cny_id[]idService6"
            },
            "serviceType": "hourprofile.HourProfileBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Worked hours"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Horaires de travail"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "false"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess7",
              "userProfileAccessId": "idUserProfileAccess7",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": false,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService7",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService7",
              "toString": "sys_cny_id[]idService7"
            },
            "serviceType": "daysoff.DaysOffBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Days off"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Jours fériés"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "false"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess9",
              "userProfileAccessId": "idUserProfileAccess9",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": false,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService9",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService9",
              "toString": "sys_cny_id[]idService9"
            },
            "serviceType": "leavevacation.leavevacationprofile.LeaveVacationProfileBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Leave Vacation profiles"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Profils de Congés et Absences"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "false"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess4",
              "userProfileAccessId": "idUserProfileAccess4",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": false,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService4",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService4",
              "toString": "sys_cny_id[]idService4"
            },
            "serviceType": "userprofile.UserProfileBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "User profile"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Profils utilisateurs"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "false"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]f6352c7e-ed05-d329-419d-f4bc2da19763",
              "userProfileAccessId": "f6352c7e-ed05-d329-419d-f4bc2da19763",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": true,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService36",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService36",
              "toString": "sys_cny_id[]idService36"
            },
            "serviceType": "expenses.ExpenseBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Expenses"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Saisie des dépenses"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "0"
              },
              {
                "associatedBundleKey": "param.def.allowRevokePostValidate.expenses",
                "associatedHelpBundleKey": "param.def.allowRevokePostValidate.expenses.help",
                "idName": "ParamAllowRevokePostValidate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "true"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": true,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "13"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess2",
              "userProfileAccessId": "idUserProfileAccess2",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": false,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService2",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService2",
              "toString": "sys_cny_id[]idService2"
            },
            "serviceType": "company.CompanyBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Companies and affiliates"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Sociétés et filiales"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "false"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "0"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          },
          {
            "id": {
              "idCompany": "2697b4b7-ea54-4cac-a57a-bce57ac146a6",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.userprofile.UserProfileAccess",
              "stringOf": "2697b4b7-ea54-4cac-a57a-bce57ac146a6[]b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3[]idUserProfileAccess8",
              "userProfileAccessId": "idUserProfileAccess8",
              "userProfileId": "b83c943b-d4a7-4ebc-b7a7-bebd6e985dc3"
            },
            "idParentItem": null,
            "idPositionItem": null,
            "ownNotion": false,
            "serviceBeanId": {
              "idCompany": "sys_cny_id",
              "idService": "idService8",
              "idUser": null,
              "parentclass": "com.azuneed.backoffice.appentity.service.ServiceBean",
              "stringOf": "sys_cny_id[]idService8",
              "toString": "sys_cny_id[]idService8"
            },
            "serviceType": "leavevacation.leavevacationtype.LeaveVacationTypeBeanServiceDef",
            "sysService": true,
            "userProfileAccessDetail": [
              {
                "extraLongDetail": null,
                "locale": "en_GB",
                "longDetail": null,
                "shortDetail": "Leave Vacation types"
              },
              {
                "extraLongDetail": null,
                "locale": "fr_FR",
                "longDetail": null,
                "shortDetail": "Types de Congés et Absences"
              }
            ],
            "userProfileParamAccess": [
              {
                "associatedBundleKey": "param.def.allowDelete.label",
                "associatedHelpBundleKey": "param.def.allowDelete.label.help",
                "idName": "AllowDelete",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowDisplay.label",
                "associatedHelpBundleKey": "param.def.allowDisplay.label.help",
                "idName": "AllowDisplay",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowGenerateExport.label",
                "associatedHelpBundleKey": "param.def.allowGenerateExport.label.help",
                "idName": "AllowGenerateExport",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "AUTHORIZATION",
                "paramValue": "false"
              },
              {
                "associatedBundleKey": "param.def.allowCreate.label",
                "associatedHelpBundleKey": "param.def.allowCreate.label.help",
                "idName": "AllowCreate",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.allowEdit.label",
                "associatedHelpBundleKey": "param.def.allowEdit.label.help",
                "idName": "AllowEdit",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              },
              {
                "associatedBundleKey": "param.def.companyVisibility.label",
                "associatedHelpBundleKey": "param.def.companyVisibility.label.help",
                "idName": "CompanyVisibility",
                "myNotion": false,
                "otherListToDisplay": null,
                "paramFamilyType": "RIGHTS",
                "paramValue": "1"
              }
            ]
          }
        ],
        "userProfileDetail": {
          "mapProperty": {
            "fr_FR": {
              "extraLongDetail": null,
              "locale": null,
              "longDetail": null,
              "shortDetail": "Administrateur"
            },
            "en_GB": {
              "extraLongDetail": null,
              "locale": null,
              "longDetail": null,
              "shortDetail": "Administrator"
            }
          }
        }
      }
    },
    null
  ];