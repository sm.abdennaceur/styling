import {User} from "./user";

export class UserAuthentication {
    token?: string;
    user?: User;
}