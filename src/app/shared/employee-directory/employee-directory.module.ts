import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeDirectoryRoutingModule } from './employee-directory-routing.module';
import {MaterialModule} from "~app/material/material.module";
import {FlexLayoutModule} from "@angular/flex-layout";


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EmployeeDirectoryRoutingModule,
    MaterialModule,
    FlexLayoutModule,
  ]
})
export class EmployeeDirectoryModule { }
