import { DOCUMENT } from "@angular/common";
import {
  Component,
  Inject,
  ElementRef,
  OnInit,
  Renderer2,
  AfterViewInit, OnDestroy,
} from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { ConfigService } from "~app/config/config.service";
import {AuthentificationService} from "~core/http/authentification.service";
import {NotificationService} from "~core/http/notification.service";
import {AuthGuard} from "~core/guards/auth.guards";
import * as _ from 'lodash';
import {UtilsService} from "~core/helpers/utils.service";
import { Subscription, timer} from "rxjs";
import {rights, serviceType,langConstant} from "~core/helpers/app-constants";
import { DirectionService } from '~core/service/direction.service';
import {LanguageService} from "~core/http/language.service";
const document: any = window.document;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit, OnDestroy{
  public config: any = {};
  userImg: string;
  homePage: string;
  isNavbarCollapsed = true;
  flagvalue :any;
  countryName:any;
  langStoreValue: string;
  defaultFlag: string;
  isOpenSidebar: boolean;
  silentLogin = false
  notificationList : any = [];
  newNotification: any = [];
  unreadedNotifCount: any
  notifCheck: any
  permissionNotif: any;
  permission: Subscription;
  currentUser: any;
  UserIN: string;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    public elementRef: ElementRef,
    private configService: ConfigService,
    private router: Router,
    private service: AuthentificationService,
    private serviceNotification: NotificationService,
    private authGuard: AuthGuard,
    private utilsService: UtilsService,
    private translateService:TranslateService,
    private directionService: DirectionService,
    private serviceLanguage: LanguageService
   
  ) {
    this.authGuard.silentLogin.subscribe(data => {
      this.silentLogin = data;
    })
// @ts-ignore
    this.currentUser = this.service.currentUserValue
    this.UserIN = this.currentUser.user?.userBean.userFirstName+' '+this.currentUser.user?.userBean.userLastName
  }
 
  listLang = [
    { text: "Français", flag: "assets/images/flags/fr.png", lang: langConstant.FR },
    { text: "English", flag: "assets/images/flags/us.jpg", lang: langConstant.EN},
    { text: "Arabic", flag: "assets/images/flags/tun.png", lang: langConstant.AR },
  ];
  ngOnInit() {
  
    this.config = this.configService.configData;
  
    this.langStoreValue = this.serviceLanguage.getCurLanguage();

    const val = this.listLang.filter((x) => x.lang === this.langStoreValue);
    this.countryName = val.map((element) => element.text);
    if (val.length === 0) {
      if (this.flagvalue === undefined) {
        this.defaultFlag = "assets/images/flags/us.jpg";
      }
    } else {
      this.flagvalue = val.map((element) => element.flag);
    }
    this.permission = this.service.Permission$.subscribe(data => {
        this.checkPermission(data);
   })
  }

  ngAfterViewInit() {
    // set theme on startup
    if (localStorage.getItem("theme")) {
      this.renderer.removeClass(this.document.body, this.config.layout.variant);
      this.renderer.addClass(this.document.body, localStorage.getItem("theme") as string);
    } else {
      this.renderer.addClass(this.document.body, this.config.layout.variant);
    }

    if (localStorage.getItem("menuOption")) {
      this.renderer.addClass(
        this.document.body,
        localStorage.getItem("menuOption") as string
      );
    } else {
      this.renderer.addClass(
        this.document.body,
        "menu_" + this.config.layout.sidebar.backgroundColor
      );
    }

    if (localStorage.getItem("choose_logoheader")) {
      this.renderer.addClass(
        this.document.body,
        localStorage.getItem("choose_logoheader") as string
      );
    } else {
      this.renderer.addClass(
        this.document.body,
        "logo-" + this.config.layout.logo_bg_color
      );
    }

    if (localStorage.getItem("sidebar_status")) {
      if (localStorage.getItem("sidebar_status") === "close") {
        this.renderer.addClass(this.document.body, "side-closed");
        this.renderer.addClass(this.document.body, "submenu-closed");
      } else {
        this.renderer.removeClass(this.document.body, "side-closed");
        this.renderer.removeClass(this.document.body, "submenu-closed");
      }
    } else {
      if (this.config.layout.sidebar.collapsed === true) {
        this.renderer.addClass(this.document.body, "side-closed");
        this.renderer.addClass(this.document.body, "submenu-closed");
      }
    }
  }
  callFullscreen() {
    if (
      !document.fullscreenElement &&
      !document.mozFullScreenElement &&
      !document.webkitFullscreenElement &&
      !document.msFullscreenElement
    ) {
      if (document.documentElement.requestFullscreen) {
        document.documentElement.requestFullscreen();
      } else if (document.documentElement.msRequestFullscreen) {
        document.documentElement.msRequestFullscreen();
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullscreen) {
        document.documentElement.webkitRequestFullscreen();
      }
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      }
    }
  }
  setLanguage(text: string, lang: string, flag: string) {
    localStorage.setItem('lang', lang)
    this.countryName = text;
    this.flagvalue = flag;
    this.langStoreValue = lang;
    this.translateService.setDefaultLang(lang);
    this.translateService.use(lang);

    if ( lang !== langConstant.AR ) {
      document.getElementById("html").removeAttribute("dir");
      this.renderer.removeClass(this.document.body, "rtl");
      this.directionService.updateDirection("ltr");
      localStorage.setItem("isRtl", 'false');
    } else if (  lang === langConstant.AR) {
      document.getElementById("html").setAttribute("dir", "rtl");
      this.renderer.addClass(this.document.body, "rtl");
      this.directionService.updateDirection("rtl");
      localStorage.setItem("isRtl", 'true');
    }

  }
  mobileMenuSidebarOpen(event: any, className: string) {
    const hasClass = event.target.classList.contains(className);
    if (hasClass) {
      this.renderer.removeClass(this.document.body, className);
    } else {
      this.renderer.addClass(this.document.body, className);
    }
  }
  callSidemenuCollapse() {
    const hasClass = this.document.body.classList.contains("side-closed");
    if (hasClass) {
      this.renderer.removeClass(this.document.body, "side-closed");
      this.renderer.removeClass(this.document.body, "submenu-closed");
    } else {
      this.renderer.addClass(this.document.body, "side-closed");
      this.renderer.addClass(this.document.body, "submenu-closed");
    }
  }
  logout() {
    this.router.navigate(['/login']);
    this.service.logout();
  }
  orderBydate(array: any) {
    return _.orderBy(array, ['date'], ['desc']);
 }
 getNotification(){
     if(!this.silentLogin){
       this.serviceNotification.getNotificationList(this.currentUser.user.userBean.id.idCompany,this.currentUser.user.userBean.id.idUser).subscribe({
         next: (data: any) => {
           if(data.code != 2000){
             this.utilsService.showSnackBar(`${data.message[0]}`, 'info-snack-bar');
           }
           this.notificationList = data.message[0];
           let base = this;
           this.newNotification =  _.filter(this.notificationList, function(o) {
             // add color and icon to object notification
             switch (o.idModule){
               case 'leave_request':
                 _.assign(o,{color: 'nfc-green', icon:"beach_access"})
                 break;
               case 'interviews':
                 _.assign(o,{color: 'nfc-blue', icon:"connect_without_contact"})
                 break;
               case 'well_being':
                 _.assign(o,{color: 'nfc-orange', icon:"tag_faces"})
                 break;
               case 'time_sheet':
                 _.assign(o,{color: 'nfc-purple', icon:"access_time"})
                 break;
               case 'user_directory':
                 _.assign(o,{color: 'nfc-red', icon:"groups"})
                 break;
               default: _.assign(o,{color: 'nfc-orange', icon:"person"})
             }
             return !o.hasBeenRead });
           this.unreadedNotifCount = this.newNotification.length;

         },
         error: (err: any) => {
           if(this.service.isAuth()){
             this.utilsService.showSnackBar(`${err.error.message[0]}`, 'error-snack-bar');
           }
         }
       })
     }
   }
   checkPermission(data : any){
    const hasAnyAuthority = this.service.getForAGivenServiceAGivenParamValue(serviceType.notificationAdminServiceDef, rights.PARAM_ALLOW_DISPLAY);
    this.permissionNotif = _.filter(data, function (o) {
       return o.name === serviceType.notificationAdminServiceDef
     });
     // check permission
     if (this.permissionNotif.length > 0 && this.permissionNotif[0].value.isActive && hasAnyAuthority > 0) {
       let time = timer(0, 4000)
       this.notifCheck = time.subscribe((t: any) => {
         this.getNotification();
       })
     }
   }

  ngOnDestroy(): void {
    if(this.notifCheck){
      this.notifCheck.unsubscribe();
    }
      this.permission.unsubscribe()
   }
   readNotification(notification: any) {
    if(!notification.hasBeenRead && this.unreadedNotifCount > 0){
       notification.hasBeenRead = true;
       this.unreadedNotifCount --;
       let dataToSend = {model: notification};
       this.serviceNotification.updateNotification(dataToSend).subscribe({
         next: (data: any) => {
           this.router.navigate([notification.urlAccess]);
           },
         error:(err: any) => {
         }
       })
     } else {
      this.router.navigate([notification.urlAccess]);
    }
   }
}
