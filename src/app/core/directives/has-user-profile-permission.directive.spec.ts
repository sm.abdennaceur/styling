import {
  Directive,
  Input,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';

import { MockBuilder, MockRender } from 'ng-mocks';

// This directive is the same as `ngIf`,
// it renders its content only when its input has truly value.
@Directive({
  selector: '[HasUserProfilePermissionTest]',
})
class HasUserProfilePermissionDirectiveTest {
  public constructor(
    protected templateRef: TemplateRef<any>,
    protected viewContainerRef: ViewContainerRef,
  ) { }

  @Input() public set HasUserProfilePermissionTest(value: any) {
    if (value) {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainerRef.clear();
    }
  }
}

describe('TestStructuralDirectiveWithoutContext', () => {
  beforeEach(() => MockBuilder(HasUserProfilePermissionDirectiveTest));
  it('hides and renders its content', () => {
    const fixture = MockRender(
      `<div *HasUserProfilePermissionTest="value">
        content
      </div>`,
      {
        value: false,
      },
    );
    expect(fixture.nativeElement.innerHTML).not.toContain('content');

    // Let's change the value to true and assert that the "content"
    // has been rendered.
    fixture.componentInstance.value = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.innerHTML).toContain('content');

    // Let's change the value to false and assert that the
    // "content" has been hidden.
    fixture.componentInstance.value = false;
    fixture.detectChanges();

    expect(fixture.nativeElement.innerHTML).not.toContain('content');
  });
});

