import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import { catchError } from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthentificationService} from '../http/authentification.service';
import { HandleErrorService } from './services/handleError.service';
@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptorService implements HttpInterceptor {

  constructor(private error: HandleErrorService) { }
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
     catchError(err => {
         if(err.status === 401){
          // auto logout if 401 response returned from api
             localStorage.removeItem('connectedUser');
             localStorage.removeItem('productAssociatedService');
             localStorage.removeItem('MapPermission');
             localStorage.removeItem('userSession');
             location.reload();
         }
      this.error.handleError(err);
       return throwError( err );
     }));
 }

}