import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-space-page',
  template: `<section class="content">
  <div class="content-block">
  <div class="block-header">
  <!-- breadcrumb -->
      <app-breadcrumb [title]="'msg.menu.mySpace'" [items]="['msg.menu.mySpace']" ></app-breadcrumb>
    </div>
    <div class="row">
    <app-dynamic-nav-menu [ListMenu]="MenuMySpace"></app-dynamic-nav-menu>
    </div>
  </div>
  </section>`,
  styles: [],
})

export class MySpacePage implements OnInit {

  MenuMySpace = [{
    label: 'msg.subMenu.overview',
    routerlink: ''
  }, {
    label: 'msg.subMenu.myfile',
    routerlink: ''
  }, {
    label: 'msg.subMenu.processingRequests',
    routerlink: ''
  }, {
    label: 'msg.subMenu.myrequests',
    routerlink: ''
  }, {
    label: 'msg.subMenu.employeeDirectory',
    routerlink: 'user'
  }]
  constructor() { }
  ngOnInit(): void {

  }

}