import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2, } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
const document: any = window.document;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

  title = 'msg.menu.accueil'
  constructor(
    private translateService: TranslateService,
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2
  ) {}

  ngOnInit() {
    let lang =  localStorage.getItem('lang');
    let isRtl = localStorage.getItem('isRtl')
    lang === null ? this.TranslateLang('fr') :  this.TranslateLang(lang)
    isRtl  === "true" ?this.setRTLSettings() :this.setLTRSettings() ;
  }
  
  TranslateLang(lang:string){
    localStorage.setItem('lang',lang)
    this.translateService.setDefaultLang(lang);
    this.translateService.use(lang);
  }
  setRTLSettings() {
    document.getElementById("html").setAttribute("dir", "rtl");
    this.renderer.addClass(this.document.body, "rtl");

    localStorage.setItem("isRtl", "true");
  }
  setLTRSettings() {
    document.getElementById("html").removeAttribute("dir");
    this.renderer.removeClass(this.document.body, "rtl");
    localStorage.setItem("isRtl", "false");
  }
}
