import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import {LoginComponent} from "./login.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MaterialModule} from "../../material/material.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import { DialogForgetPasswordComponent } from './dialog-forget-password/dialog-forget-password.component';
import { TranslateModule } from '@ngx-translate/core';
import { RetrievepwdComponent } from './retrievepwd/retrievepwd.component';
import { MatPasswordStrengthModule } from "@angular-material-extensions/password-strength";
@NgModule({
  declarations: [LoginComponent, DialogForgetPasswordComponent, RetrievepwdComponent],
  exports: [LoginComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    TranslateModule,
    MatPasswordStrengthModule.forRoot()
  ]
})
export class LoginModule { }
