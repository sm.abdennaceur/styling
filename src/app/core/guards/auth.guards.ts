import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Injectable } from "@angular/core";
import { AuthentificationService } from "../http/authentification.service";
import { BehaviorSubject } from "rxjs";


@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
    currentUser: any;
    silentLogin = new BehaviorSubject(false);
    constructor(private router: Router, private authenticationService: AuthentificationService) {
    }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.currentUser = this.authenticationService.currentUserValue;
        if (this.currentUser.user && state["root"].queryParams.iframe === undefined) {
            // logged in so return true
            return true;
        } else if (state["root"].queryParams.token && state["root"].queryParams.email && state["root"].queryParams.redirectUri) {
            this.silentLogin.next(true)
            if (!this.currentUser.user) {
                // silent login
                this.authenticationService.getData(`logon/noSession/auth/legacy?mail=${decodeURIComponent(state["root"].queryParams.email)}&token=${decodeURIComponent(state["root"].queryParams.token)}`).subscribe({
                    next: (data: any) => {
                        //permission
                        this.authenticationService.setCurrentUser(data.message);
                        this.authenticationService.initProfileMenu();
                        this.router.navigate([decodeURIComponent(state["root"].queryParams.redirectUri)])
                    },
                    error: (err) => {
                        console.log('err', err);
                    }
                });
            }
            return true
        }
        else {
            // not logged in so redirect to login page with the return url
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
            return true;
        }
    }

    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let resultOF = false
        const service = typeof childRoute.data.service === 'string' ? [<string>childRoute.data.service] : <string[]>childRoute.data.service;
        const role = childRoute.data.role;
        if (service && role && this.authenticationService.hasAnyServicesDirect(service) && this.authenticationService.isAuth() && this.authenticationService.isAbleTo()) {
            resultOF = true
        } else if (service && this.authenticationService.hasAnyServicesDirect(service) && this.authenticationService.isAuth()) {
            resultOF = true;
        } else if (role && this.authenticationService.isAbleTo() && this.authenticationService.isAuth()) {
            if (this.authenticationService.isAbleTo() && this.authenticationService.isAuth()) {
                resultOF = true;
            }
        }
        return resultOF;
    }
}