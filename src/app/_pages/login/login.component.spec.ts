import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';



import { RouterTestingModule } from '@angular/router/testing';
import { AuthentificationService } from '~core/http/authentification.service';
import { blankUser, testUserData, validUser } from '~app/mocks';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { of } from 'rxjs';

import { UtilsService } from '~core/helpers/utils.service';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MatCheckboxModule } from '@angular/material/checkbox';

const routerSpy = jasmine.createSpyObj('Router', ['getCurrentNavigation', 'navigate']);
const activatedRouteStub = {
  snapshot: {
    queryParams: {
      returnUrl: '/home',
    }
  }
};
const loginServiceSpy = jasmine.createSpyObj('AuthentificationService', ['login']);
loginServiceSpy.login.and.returnValue(of(testUserData))
const utilsService = jasmine.createSpyObj('UtilsService', ['utilsService']);

const ModalService = jasmine.createSpyObj('MatDialog', ['MatDialog']);

 
const loginErrorMsg = 'Invalid Login';


describe('Login Component Isolated Test', () => {
  let component: LoginComponent;

  beforeEach(() => {
    component = new LoginComponent(new FormBuilder(), routerSpy, new ActivatedRoute(), loginServiceSpy, ModalService, utilsService);
  });

  function updateForm(userEmail: any, userPassword: any) {
    component.loginForm.controls['email'].setValue(userEmail);
    component.loginForm.controls['password'].setValue(userPassword);
    component.loginForm.controls['connected'].setValue(false);
  }

  it('Component successfully created', () => {
    expect(component).toBeTruthy();
  });

  it('component initial state', () => {
    // expect(component.submitted).toBeFalsy();
    expect(component.loginForm).toBeDefined();
    expect(component.loginForm.invalid).toBeTruthy();
    //   expect(component.authError).toBeFalsy();
    //   expect(component.authErrorMsg).toBeUndefined();
  });

  it('submitted should be true when onSubmit()', () => {
    component.onSubmit();
    //expect(component.submitted).toBeTruthy();
    // expect(component.authError).toBeFalsy();
  });

  it('form value should update from when u change the input', (() => {
    updateForm(validUser.email, validUser.password);
    expect(component.loginForm.value).toEqual(validUser);
  }));

  it('Form invalid should be true when form is invalid', (() => {
    updateForm(blankUser.email, blankUser.password);
    expect(component.loginForm.invalid).toBeTruthy();
  }));

});

describe('Login Component Shallow Test', () => {

  let fixture: ComponentFixture<LoginComponent>;

  function updateForm(userEmail: any, userPassword: any, connected: boolean) {
    fixture.componentInstance.loginForm.controls['email'].setValue(userEmail);
    fixture.componentInstance.loginForm.controls['password'].setValue(userPassword);
    fixture.componentInstance.loginForm.controls['connected'].setValue(connected);
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatDialogModule,
        MatSnackBarModule,
        MatCheckboxModule,
        TranslateModule.forRoot()],
      providers: [
        { provide: AuthentificationService, useValue: loginServiceSpy },
        FormBuilder,
        ReactiveFormsModule,
        { provide: Router, useValue: routerSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [LoginComponent],
    }).compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
  }));

  it('should create', () => {
    expect(fixture).toBeTruthy();
  });
  it('created a form with username and password input and login button', () => {
    // const fixture = TestBed.createComponent(LoginComponent);
    const usernameContainer = fixture.debugElement.nativeElement.querySelector('#username-container');
    const passwordContainer = fixture.debugElement.nativeElement.querySelector('#password-container');
    const loginBtnContainer = fixture.debugElement.nativeElement.querySelector('#login-btn-container');
    expect(usernameContainer).toBeDefined();
    expect(passwordContainer).toBeDefined();
    expect(loginBtnContainer).toBeDefined();
  });

  it('Display Username Error Msg when Username is blank', () => {
    updateForm(blankUser.email, validUser.password, validUser.connected);
    fixture.detectChanges();

    const button = fixture.debugElement.nativeElement.querySelector('#buttonSave');
    button.click();
    fixture.detectChanges();

    // const usernameErrorMsg = fixture.debugElement.nativeElement.querySelector('#username-error-msg');
    // expect(usernameErrorMsg).toBeDefined();
    // expect(usernameErrorMsg.innerHTML).toContain('Please enter username');
  });

  it('Display Password Error Msg when Username is blank', () => {
    updateForm(validUser.email, blankUser.password, validUser.connected);
    fixture.detectChanges();

    const button = fixture.debugElement.nativeElement.querySelector('#buttonSave');
    button.click();
    fixture.detectChanges();

    // const passwordErrorMsg = fixture.debugElement.nativeElement.querySelector('#password-error-msg');
    // expect(passwordErrorMsg).toBeDefined();
    // expect(passwordErrorMsg.innerHTML).toContain('Please enter password');
  });

  it('Display Both Username & Password Error Msg when both field is blank', () => {
    updateForm(blankUser.email, blankUser.password, validUser.connected);
    fixture.detectChanges();

    const button = fixture.debugElement.nativeElement.querySelector('#buttonSave');
    button.click();
    fixture.detectChanges();

    // const usernameErrorMsg = fixture.debugElement.nativeElement.querySelector('#username-error-msg');
    // const passwordErrorMsg = fixture.debugElement.nativeElement.querySelector('#password-error-msg');

    // expect(usernameErrorMsg).toBeDefined();
    // expect(usernameErrorMsg.innerHTML).toContain('Please enter username');

    // expect(passwordErrorMsg).toBeDefined();
    // expect(passwordErrorMsg.innerHTML).toContain('Please enter password');
  });

  it('When username is blank, username field should display red outline ', () => {
    updateForm(blankUser.email, validUser.password, validUser.connected);
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('#buttonSave');
    button.click();
    fixture.detectChanges();

    // const inputs = fixture.debugElement.nativeElement.querySelectorAll('input');
    // const usernameInput = inputs[0];

    // expect(usernameInput.classList).toContain('is-invalid');
  });

  it('When password is blank, password field should display red outline ', () => {
    updateForm(validUser.email, blankUser.password, validUser.connected);
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('#buttonSave');
    button.click();
    fixture.detectChanges();

    // const inputs = fixture.debugElement.nativeElement.querySelectorAll('input');
    // const passwordInput = inputs[1];

    // expect(passwordInput.classList).toContain('is-invalid');
  });

});

describe('Login Component Integrated Test', () => {
  let fixture: ComponentFixture<LoginComponent>;
  let loginSpy;
  function updateForm(userEmail:any, userPassword:any) {
    fixture.componentInstance.loginForm.controls['email'].setValue(userEmail);
    fixture.componentInstance.loginForm.controls['password'].setValue(userPassword);
    fixture.componentInstance.loginForm.controls['connected'].setValue(false);
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatDialogModule,
        MatSnackBarModule,
        MatCheckboxModule,
        TranslateModule.forRoot()],
      providers: [
        { provide: AuthentificationService, useValue: loginServiceSpy },
        FormBuilder,
        ReactiveFormsModule,
        { provide: Router, useValue: routerSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [LoginComponent],
    }).compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
  }));

  it('loginService login() should called ', fakeAsync(() => {
    updateForm(validUser.email, validUser.password);
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('#buttonSave');
    button.click();
    fixture.detectChanges();

    expect(loginServiceSpy.login).toHaveBeenCalled();
  }));

  it('should route to home if login successfully', fakeAsync(() => {
    updateForm(validUser.email, validUser.password);
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('#buttonSave');
    button.click();
    advance(fixture);

    loginSpy = loginServiceSpy.login.and.returnValue(of(testUserData));

    advance(fixture);

    expect(routerSpy.navigate).toHaveBeenCalled();
    const navArgs = '/home';
    // expecting to navigate to id of the component's first hero
    expect(navArgs).toBe('/home', 'should nav to Home Page');
  }));
  function advance(f: ComponentFixture<any>) {
    tick();
    f.detectChanges();
  }
});