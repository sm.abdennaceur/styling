import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AideRoutingModule } from './aide-routing.module';
import { AidePage } from './page/aide.page';
import { MaterialModule } from 'src/app/material/material.module';
 


@NgModule({
  declarations: [
    AidePage
  ],
  imports: [
    CommonModule,
    AideRoutingModule,
    MaterialModule
  ]
})
export class AideModule { }
