 
export const ROUTES: any[] = [
  {
    path: "/home",
    title: "msg.menu.accueil",
    moduleName: "",
    iconType: "feather",
    icon: "home",
    class: "",
    groupTitle: true,
    badge: "",
    badgeClass: "",
    role: [],
    submenu: [],
  },
  {
    path: "/my-space",
    title: "msg.menu.mySpace",
    moduleName: "",
    iconType: "feather",
    icon: "user",
    class: "",
    groupTitle: true,
    badge: "",
    badgeClass: "",
    role: [],
    submenu: [],
  },

  {
    path: "/my-team",
    title: "msg.menu.myCrew",
    moduleName: "",
    iconType: "feather",
    icon: "users",
    class: "",
    groupTitle: true,
    badge: "",
    badgeClass: "",
    role: [],
    submenu: [],
  },

  {
    path: "/my-company",
    title: "msg.menu.myCompany",
    moduleName: "",
    iconType: "mat",
    icon: "business",
    class: "",
    groupTitle: true,
    badge: "",
    badgeClass: "",
    role: [],
    submenu: [],
  }

  
];
