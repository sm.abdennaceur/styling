import { Injectable } from '@angular/core';
import {BaseService} from "~core/http/base.service";
import {HttpClient} from "@angular/common/http";
import {Filter} from "~core/helpers/app-constants";
import {map} from "rxjs/operators";
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UserService  extends BaseService{
  BaseUrl = environment.BaseUrlBack
  filterUser = {} as Filter
// @ts-ignore
  constructor(public http: HttpClient) {
    super(http)
  }
  initializeFilter(){
    this.filterUser = {
      idCompany :[],
      idUser : [],
      idCompanyOrganization: [],
      status: [],
      analyticAxeItem: [],
      userDirectoryTagId: [],
      userWorkCategory: [],
      limit: [],
      offset: [],
      searchInput:[],
      planType: [],
      companyType: [],
      idCompanyPartner: [],
      withoutUserDirectory: []
    }
    return this.filterUser
  }
  getUsers(filters: Filter){
    return this.getData(`admin/users`, {filters:JSON.stringify(filters)})
        .pipe(map(data => {
          return data;
        }));
  }
  getFilterContent(filters: Filter){
    return this.getData(`admin/users/filters`, {filters:JSON.stringify(filters)})
        .pipe(map(data => {
          return data;
        }));
  }
  exportPDF(filters: Filter){
    return this.getData(`admin/users/export/staffRegister`, {filters:JSON.stringify(filters)})
        .pipe(map(data => {
          return data;
        }));
  }
  exportXLS(filters: Filter){
    return this.getData(`admin/users/export/xls`, {filters:JSON.stringify(filters)})
        .pipe(map(data => {
          return data;
        }));
  }
  __exportData (data : any){

    if(data.message[0].indexOf('/') != -1 || data.message[0].indexOf('\\') != -1)
      window.open(this.BaseUrl + data.message[0], '_parent', '');
    else
    {
      /* if(MessageService)
       {
           MessageService.showRestMessage(data);
       }
       $window.open(data.message[1], '_parent', '');*/
    }


  }
  deleteUser(idUser : any){
    return this.delete('admin/user/' + idUser) .pipe(map(data => {
      return data;
    }));
  }
}
