
import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, HostBinding, OnInit } from '@angular/core';
import {AuthGuard} from "~core/guards/auth.guards";
@Component({
  selector: 'layout-default',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  sideBarOpen = false;
  leftBarOpen = false;
  @HostBinding('class') className = '';
  silentLogin = false

  
  constructor(private overlay: OverlayContainer, private authGuard: AuthGuard) {
    this.authGuard.silentLogin.subscribe(data => {
      this.silentLogin = data;
    })
  }

  ngOnInit() {
  }

  sideBarToggler($event: any) {
    this.sideBarOpen = !this.sideBarOpen;
  }

  letBarToggler($event: any) {
    this.leftBarOpen = !this.leftBarOpen;
  }

  DarkModechecked($event: boolean) {
    const darkClassName = 'darkMode';
    this.className = $event ? darkClassName : '';
    if ($event) {
      this.overlay.getContainerElement().classList.add(darkClassName);
      document.documentElement.classList.add('dark')
    } else {
      this.overlay.getContainerElement().classList.remove(darkClassName);
      document.documentElement.classList.remove('dark')
    }

  }

}
