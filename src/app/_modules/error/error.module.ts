import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { ErrorRoutes } from './error.routing';
import { Error500Component } from './error500/error500.component';
import { Error404Component } from './error404/error404.component';

@NgModule({
  declarations: [
    Error500Component,
    Error404Component
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ErrorRoutes)
  ]
})
export class ErrorModule { }
