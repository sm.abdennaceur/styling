import { Injectable } from '@angular/core';
import {map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {BaseService} from "~core/http/base.service";

@Injectable({
  providedIn: 'root'
})
export class NotificationService extends BaseService{
// @ts-ignore
  constructor(public  http: HttpClient) {
    super(http)
  }
  getNotificationList(idCompany: any, userId: any){
    return this.getData(`notifications?idCompany=${idCompany}&idUser=${userId}`)
        .pipe(map(data => {
          return data;
        }));
  }
    updateNotification(data: any){
        return this.create(`notification`,this.prepareData(data),'')
            .pipe(map(data => {
                return data;
            }));
    }
}
