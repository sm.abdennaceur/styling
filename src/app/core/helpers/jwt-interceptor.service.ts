import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import { AuthentificationService } from '~core/http/authentification.service';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService implements HttpInterceptor {
  constructor(private authenticationService: AuthentificationService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      withCredentials: true,
      setHeaders: {
        'Access-Control-Allow-Origin':'*',
        'Accept':'application/json, text/plain, */*',
        'Vary':'Origin',

      }
    });
    // add authorization header with jwt token if available
    const currentUser = this.authenticationService.currentUserValue;
    const isLoggedIn = currentUser && currentUser.token;
    if (currentUser && isLoggedIn) {
      request = request.clone({
        setHeaders: {
          //'Content-Type': 'application/json',
          'X-Auth-Token': `${currentUser.token}`
        }
      });
    }
    return next.handle(request);
  }
}
