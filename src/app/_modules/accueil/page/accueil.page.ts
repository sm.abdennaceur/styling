

import { Component, OnInit } from "@angular/core";
import {AuthGuard} from "~core/guards/auth.guards";
import {UserService} from "~core/http/user.service";
import {AuthentificationService} from "~core/http/authentification.service";

import { MatDialog } from "@angular/material/dialog";
import { DynamicFormComponent } from "~shared/dynamic-form/dynamic-form.component";

@Component({
    selector: 'app-Accueil-page',
    template: `<section [class]="!silentLogin?'content':''">
    <div class="content-block">
    <div class="block-header" *ngIf="!silentLogin">
    <!-- breadcrumb -->
    <app-breadcrumb  [title]="'msg.menu.accueil'" [items]="['msg.menu.accueil']" [active_item]="">
    </app-breadcrumb>
  </div>
       <div [class]="!silentLogin?'row':''">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="card">
               <div class="header">
                   <h2>{{'msg.home.widget.example.title' | translate}}</h2>
                   <button mat-button [matMenuTriggerFor]="menu" class="header-dropdown">
                       <mat-icon>more_vert</mat-icon>
                   </button>
                   <mat-menu #menu="matMenu">
                       <button mat-menu-item>Action</button>
                       <button mat-menu-item>Another action</button>
                       <button mat-menu-item>Something else here</button>
                   </mat-menu>
               </div>
               <div class="body">
                   <div class="row">
                       <div class="col-md-12 col-xl-4 d-flex flex-column justify-content-center">
                           <div class="m-l-10 m-b-20">
                               <h1>$1,17,289</h1>
                               <h4 class="font-weight-light col-green">{{"msg.home.widget.example.total.income" | translate}}</h4>
                               <p class="mb-2">{{ "msg.home.widget.example.total.income.text"| translate}}
                               </p>
                           </div>
                           <div class="m-l-10">
                               <h1>$65,984</h1>
                               <h4 class="font-weight-light col-orange">{{"msg.home.widget.example.total.expense"| translate}}</h4>
                               <p class="mb-2"> {{ "msg.home.widget.example.total.expense.text"| translate}}
                               </p>
                           </div>
                       </div>
                       <div class="col-md-12 col-xl-8">
                           <div class="row">
                               <div class="col-md-6">
                                   <h5 class="tx-primary m-b-30">{{'msg.home.widget.example.title.country'| translate}}</h5>
                                   <div class="mb-3 mt-3">
                                       <table class="table table-borderless">
                                           <tbody>
                                           <tr class="tr-height-m">
                                               <td class="text-muted">{{"msg.home.widget.example.india" | translate}}</td>
                                               <td class="w-100 px-0">
                                                   <mat-progress-bar mode="determinate" class="progress-m progress-round progress-shadow"
                                                                     value="40">
                                                   </mat-progress-bar>
                                               </td>
                                               <td>
                                                   <h5 class="font-weight-bold mb-0">154</h5>
                                               </td>
                                           </tr>
                                           <tr class="tr-height-m">
                                               <td class="text-muted">{{"msg.home.widget.example.usa" | translate}}</td>
                                               <td class="w-100 px-0">
                                                   <mat-progress-bar mode="determinate"
                                                                     class="progress-m progress-round green-progress progress-shadow" value="40">
                                                   </mat-progress-bar>
                                               </td>
                                               <td>
                                                   <h5 class="font-weight-bold mb-0">423</h5>
                                               </td>
                                           </tr>
                                           <tr class="tr-height-m">
                                               <td class="text-muted">{{"msg.home.widget.example.shrilanka" | translate}}</td>
                                               <td class="w-100 px-0">
                                                   <mat-progress-bar mode="determinate"
                                                                     class="progress-m progress-round sky-progress progress-shadow" value="40">
                                                   </mat-progress-bar>
                                               </td>
                                               <td>
                                                   <h5 class="font-weight-bold mb-0">265</h5>
                                               </td>
                                           </tr>
                                           <tr class="tr-height-m">
                                               <td class="text-muted">{{"msg.home.widget.example.australia" | translate}}</td>
                                               <td class="w-100 px-0">
                                                   <mat-progress-bar mode="determinate"
                                                                     class="progress-m progress-round orange-progress progress-shadow" value="40">
                                                   </mat-progress-bar>
                                               </td>
                                               <td>
                                                   <h5 class="font-weight-bold mb-0">341</h5>
                                               </td>
                                           </tr>
                                           <tr class="tr-height-m">
                                               <td class="text-muted">{{"msg.home.widget.example.japan" | translate}}</td>
                                               <td class="w-100 px-0">
                                                   <mat-progress-bar mode="determinate"
                                                                     class="progress-m progress-round red-progress progress-shadow" value="40">
                                                   </mat-progress-bar>
                                               </td>
                                               <td>
                                                   <h5 class="font-weight-bold mb-0">238</h5>
                                               </td>
                                           </tr>
                                           <tr class="tr-height-m">
                                               <td class="text-muted">{{"msg.home.widget.example.italy" | translate}}</td>
                                               <td class="w-100 px-0">
                                                   <mat-progress-bar mode="determinate" class="progress-m progress-round progress-shadow"
                                                                     value="40">
                                                   </mat-progress-bar>
                                               </td>
                                               <td>
                                                   <h5 class="font-weight-bold mb-0">153</h5>
                                               </td>
                                           </tr>
                                           </tbody>
                                       </table>
                                   </div>
                               </div>
                               <div class="col-md-6 mt-3">
                                   <!-- chart -->
                                   <div class="recent-report__chart">
                                       <canvas baseChart class="chart" [data]="doughnutChartData" [labels]="doughnutChartLabels"
                                               [options]="doughnutChartOptions" [legend]="doughnutChartLegend" [chartType]="doughnutChartType"
                                               [colors]="doughnutChartColors"></canvas>
                                   </div>
                                   <div class="country-chart">
                                       <div class="d-flex justify-content-between mx-xl-5 mt-3">
                                           <div class="chart-note">
                                               <span class="dot dot-product bg-green"></span>
                                               <span>Itely</span>
                                           </div>
                                           <p class="mb-0">$30,289</p>
                                       </div>
                                       <div class="d-flex justify-content-between mx-xl-5 mt-3">
                                           <div class="chart-note">
                                               <span class="dot dot-product bg-orange"></span>
                                               <span>USA</span>
                                           </div>
                                           <p class="mb-0">$25,968</p>
                                       </div>
                                       <div class="d-flex justify-content-between mx-xl-5 mt-3">
                                           <div class="chart-note">
                                               <span class="dot dot-product bg-purple"></span>
                                               <span>India</span>
                                           </div>
                                           <p class="mb-0">$45,278</p>
                                       </div>
                                       <div class="d-flex justify-content-between mx-xl-5 mt-3">
                                           <div class="chart-note">
                                               <span class="dot dot-product bg-red"></span>
                                               <span>Shrilanka</span>
                                           </div>
                                           <p class="mb-0">$21,376</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
     </div>
        </div>
        </section>
        `,

    styles: [],
})

export class AccueilPage implements OnInit {
    filterAnnuaire: any
    currentUser: any
    // Doughnut chart start
    public doughnutChartLabels: string[] = ["India", "USA", "Itely", "Shrilanka"];
    public doughnutChartData: number[] = [22, 31, 28, 19];
    public doughnutChartLegend = false;
    public doughnutChartColors: any[] = [
        {
            backgroundColor: ["#735A84", "#E76412", "#9BC311", "#DC3545"],
        },
    ];
    public doughnutChartType = "doughnut";
    public doughnutChartOptions: any = {
        animation: false,
        responsive: true,
    };
    silentLogin = false
    constructor(
        private authGuard: AuthGuard,
         private userService: UserService,
        private service: AuthentificationService,
        public dialog: MatDialog,) {
        this.currentUser = this.service.currentUserValue
       /* this.filterAnnuaire = this.userService.initializeFilter();
        this.filterAnnuaire.limit.push(25)
        this.filterAnnuaire.offset.push(0)
        this.filterAnnuaire.idCompany.push(this.currentUser.user.userBean.id.idCompany)
        this.userService.getUsers(this.filterAnnuaire).subscribe(data =>{
            console.log('data ',data);
        })
        this.userService.getFilterContent(this.filterAnnuaire).subscribe(data =>{
            console.log('data filter ',data);
        })
        this.userService.exportPDF(this.filterAnnuaire).subscribe(data =>{
            console.log('data PDF ',data);
            this.userService.__exportData(data)
        })
        this.userService.exportXLS(this.filterAnnuaire).subscribe(data =>{
            console.log('data xls ',data);
            this.userService.__exportData(data)
        })*/
        this.authGuard.silentLogin.subscribe(data => {
            this.silentLogin = data;

        })
    }
    ngOnInit() {
    }

}