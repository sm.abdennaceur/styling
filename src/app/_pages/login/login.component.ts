import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthentificationService } from "../../core/http/authentification.service";
import {MatDialog} from "@angular/material/dialog";
import {DialogForgetPasswordComponent} from "~app/_pages/login/dialog-forget-password/dialog-forget-password.component";
import {UtilsService} from "~core/helpers/utils.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit, AfterViewInit {
  @ViewChild('emailInput') emailInputElment: ElementRef;

  adminLogin: boolean = false;
  loginForm: FormGroup;
  showEror = false;
  show = false;
  error = '';
  returnUrl: string;
  envName = environment.name
  imagePath = environment.imageLogo
  altImage = environment.altImage
  showPwd = true;
  loading = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private service: AuthentificationService,
    private dialog: MatDialog,
    private utilsService: UtilsService

  ) { 
    this.loginForm = this.formBuilder.group({
      email: ['',[ Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      connected: new FormControl(false)
    });
  }
  ngAfterViewInit(): void {
   // this.emailInputElment.nativeElement.focus();
  }

  ngOnInit() {
    // this.adminLogin = this.route.snapshot.data.admin;
   
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/home';
    const currentUser = this.service.currentUserValue;
    if (currentUser) {
      this.router.navigate([this.returnUrl]);
    }
  }
  hasError(controlName: string, errorName: string) {
    return this.loginForm.controls[controlName].hasError(errorName);
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  toggleShow() {
    this.show = !this.show;
  }
  showPassword(){
    this.showPwd = !this.showPwd;
  }

  onSubmit() {
    this.loading = true;
    this.service.login(this.f.email.value, this.f.password.value, this.f.connected.value)
      .pipe(first())
      .subscribe({
        next: (data) => {
          if(data && data.code === 4000){
            this.utilsService.showSnackBar(`${data.message}`, 'error-snack-bar');
            this.loading = false;

          } else {
            this.loading = false;
            this.router.navigate([this.returnUrl]);
          }

          },
        error: (err) => {
          this.utilsService.showSnackBar(`${err.message}`, 'error-snack-bar');
          this.loading = false;
        }
      });
  }

  openPwdDialog() {
    const dialogRef = this.dialog.open(DialogForgetPasswordComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
      if (result) {
        console.log(result);
        this.initPwd(result.email.value)
      }
    });
  }
  initPwd(email: any){
console.log(email)
    this.service.resetPwdRequest(email).subscribe({
      next: (data: any) => {
        this.utilsService.showSnackBar(`${data.message[0]}`, 'info-snack-bar');
      },
      error: (err) => {
        this.utilsService.showSnackBar(`${err.message}`, 'error-snack-bar');
      }
    })
  }

  directMicrosoftConnectOrRedirect() {

  }
}
