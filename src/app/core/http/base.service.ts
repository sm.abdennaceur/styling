import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "~environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  apiUrl = environment.apiUrl
  constructor(public http: HttpClient) {

  }
  getData(route: string, params?: any) {
    return this.http.get(this.createCompleteRoute(route, this.apiUrl), {params: params});
  }
  create(route: string, body: any, additionnalHeaders : any) {
    //let header = new HttpHeaders(body)
    let header = new HttpHeaders(additionnalHeaders)
    return this.http.post<any>(this.createCompleteRoute(route, this.apiUrl), body,{headers: header});
  }

  update(route: string, body: any) {
    return this.http.put(this.createCompleteRoute(route, this.apiUrl), body);
  }

  patch(route: string, body: any){
    return this.http.patch(this.createCompleteRoute(route, this.apiUrl), body);
  }

  delete(route: string) {
    return this.http.delete(this.createCompleteRoute(route, this.apiUrl));
  }

  public createCompleteRoute = (route: string, envAddress: string) => {
    return `${envAddress}/${route}`;
  }
  public getCompleteRoute = (route: string) => {
    return `${this.apiUrl}/${route}`;
  }
  public prepareData = (data : any) =>
  {
    console.log(data)
    let formData = new FormData();
    console.log(JSON.parse(JSON.stringify(data.model)));
    // need to convert our json object to a string version of json otherwise
    // the browser will do a 'toString()' on the object which will result
    // in the value '[Object object]' on the server.
    formData.append("jsonData", JSON.stringify(data.model));
    console.log(formData.get('jsonData'))
    // now add all of the assigned files
    if(data.jsonFilesLocaleName)
    {
      formData.append("jsonFilesLocaleName",data.jsonFilesLocaleName);

    }
    /*if(data.files)
    {
      for (var i = 0; i < data.files.length; i++)
      {
        // add each file to the form data and iteratively name them
        formData.append("file", data.files[i], __base64UrlEncode(data.files[i].name));
      }
    }


    // now add all of the assigned files
    if(data.workContractFiles)
    {
      for (var i = 0; i < data.workContractFiles.length; i++)
      {
        // add each file to the form data and iteratively name them
        formData.append("workContractFiles", data.workContractFiles[i], __base64UrlEncode(data.workContractFiles[i].name));
      }
    }*/
    console.log(formData)
    return formData;
  }
}
