import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login.component";
import {RetrievepwdComponent} from "~app/_pages/login/retrievepwd/retrievepwd.component";

const routes: Routes = [
  { path: '',
    component: LoginComponent},
    {path: 'retrievepwd', component: RetrievepwdComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
