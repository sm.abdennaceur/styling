import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FilterItem } from '~core/helpers/app-constants';
 
@Injectable({
    providedIn: 'root'
})
export class FilterService {
    public filterSubject: BehaviorSubject<any> = new BehaviorSubject({});
    public currentFilter$ = this.filterSubject.asObservable();


    public initializeFilterSubject: BehaviorSubject<any> = new BehaviorSubject(Filter);
    public initializeFilter$ = this.initializeFilterSubject.asObservable();

    constructor() {
        const CurrentFilter = localStorage.getItem('filter');
        this.setCurrentFilter(CurrentFilter == null ? null : JSON.parse(CurrentFilter));
    }

    setCurrentFilter = (data: any) => {
        localStorage.setItem('filter', JSON.stringify(data));
        this.filterSubject.next(data);
    };
    public get currentFilterValue(): any {
        // @ts-ignore
        return this.filterSubject.value != null ? this.filterSubject.value : JSON.parse(localStorage.getItem('filter'))
    }

    clearData() {
        localStorage.removeItem('filter');
        this.filterSubject.next(null);
    }

    updateFilter(data: any) {
        this.setCurrentFilter(data)
    }
    getObjKey(value: string) {
        //@ts-ignore
        return Object.keys(FilterItem).find(key => FilterItem[key] === value);
    }

    setinitializeFilterSubject(data: any) {
        console.log('data',data)
        const { idCompany,
            idUser,
            idCompanyOrganization,
            status,
            analyticAxeItem,
            userDirectoryTagId,
            userWorkCategory,
            limit,
            offset,
            searchInput,
            planType,
            companyType,
            idCompanyPartner,
            withoutUserDirectory,
            appRole
        } = data
        if (idCompany) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), idCompany })
        }
        if (idUser) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), idUser })
        }
        if (idCompanyOrganization) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), idCompanyOrganization })
        }
        if (status) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), status })
        }
        if (analyticAxeItem) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), analyticAxeItem })
        }
        if (userDirectoryTagId) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), userDirectoryTagId })
        }
        if (userWorkCategory) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), userWorkCategory })
        }
        if (limit) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), limit })
        }
        if (offset) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), offset })
        }
        if (searchInput) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), searchInput })
        }
        if (planType) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), planType })
        }
        if (companyType) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), companyType })
        }
        if (idCompanyPartner) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), idCompanyPartner })
        }
        if (withoutUserDirectory) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), withoutUserDirectory })
        }
        if (appRole) {
            this.initializeFilterSubject.next({ ...this.getInitFilter(), appRole })
        }
    }
    getInitFilter() {
        return this.initializeFilterSubject.value
    }

}
export const Filter = {
    idCompany: [],
    idUser: [],
    idCompanyOrganization: [],
    status: [],
    analyticAxeItem: [],
    userDirectoryTagId: [],
    userWorkCategory: [],
    limit: [],
    offset: [],
    searchInput: [],
    planType: [],
    companyType: [],
    idCompanyPartner: [],
    withoutUserDirectory: [],
    appRole:[]
}

