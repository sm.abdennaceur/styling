import {Injectable, Injector} from '@angular/core';
import {AuthentificationService} from "~core/http/authentification.service";

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  connectUser: any
  constructor() {
  }

  getCurLanguage(){
    let lang: string | null ="";
    if(localStorage.getItem('connectedUser') && localStorage.getItem('lang')){
      this.connectUser = JSON.parse(<string>localStorage.getItem('connectedUser'))
      lang = this.connectUser[1].sessionLocale
    } else {
      lang = navigator.language
    }
    if (lang && (lang.length >= 2))
      return lang.slice(0,2).toLowerCase();

    return 'en'; // default
  }
}
