export class ProfilePermission {
    name?: string;
    value?: Permission
    permissionStatus?:boolean
};

export class Permission {
    isActive: boolean;
    isDisabled: boolean;
    isASuscribedModule: boolean;
    validTill: string;
    activateDate: string;
    permissionCode: string
}