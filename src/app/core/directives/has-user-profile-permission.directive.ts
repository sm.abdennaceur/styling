import {
  Directive,
  Input,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { AuthentificationService } from '~core/http/authentification.service';

/**
* @whatItDoes Conditionally includes an HTML element if current user has not any
* of the authorities passed as the `expression`.
*
* @howToUse
* ```
*     
*     <some-element *HasUserProfilePermission="['serviceType' ,'permission','permissionCode','operateurCode']"">...</some-element>
*
* ```
*/
@Directive({
  selector: '[HasUserProfilePermission]',
})
export class HasUserProfilePermissionDirective {

  serviceType: string;
  permission: string;
  permissionCode: string;
  /***
   * if (operateurCode ==== 'lower' ) operateur = '>'
   * if (operateurCode ==== 'upper' ) operateur = '<'
   * if (operateurCode ==== 'equivalent' ) operateur = '==='
   * 
   */
  operateurCode: string;

  constructor(
    private viewContainerRef: ViewContainerRef,
    private templateRef: TemplateRef<any>,
    private authService: AuthentificationService,
  ) { }
  @Input()
  set HasUserProfilePermission(value: string[]) {
    this.serviceType = value[0];
    this.permission = value[1];
    this.permissionCode = value[2];
    this.operateurCode = value[3]
    this.updateView();
  }


  updateView(): void {
    const hasAnyAuthority = this.authService.getForAGivenServiceAGivenParamValue(this.serviceType, this.permission);
    if (hasAnyAuthority) {
      let result = this.operator(hasAnyAuthority, parseInt(this.permissionCode), this.operateurCode);
      if (result) {
        this.viewContainerRef.createEmbeddedView(this.templateRef);
      }
    }
  }

  operator(hasAnyAuthority: number, permissionCode: number, operateurCode: string) {
    switch (operateurCode) {
      case 'lower':
        return hasAnyAuthority <= permissionCode
      case 'upper':
        return hasAnyAuthority >= permissionCode
      default:
        return hasAnyAuthority === permissionCode
    }
  }
}
